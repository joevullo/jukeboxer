package uk.ac.brookes.jvullo11027969.music_player.music_fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

import uk.ac.brookes.jvullo11027969.main_activity.JukeboxerMainActivity;
import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AsyncMusicDBConnector;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AsyncMusicDBQuery;
import uk.ac.brookes.jvullo11027969.music_player.music_interface.MusicTabManagerFragment;
import uk.ac.brookes.jvullo11027969.music_player.music_interface.SelectiveMusicList;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.AbstractMusic;
import uk.ac.brookes.jvullo11027969.utils.Constants;
import uk.ac.brookes.jvullo11027969.utils.SpecialSelectableAdapter;

/**
 * A fragment representing a list of Items. This is the base
 *
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
abstract public class AbstractMusicBaseFragment extends Fragment implements ListView.OnItemClickListener ,
        AbsListView.MultiChoiceModeListener , AsyncMusicDBConnector.MusicReceivedListener, AsyncMusicDBConnector.MusicDataSubmittedListener {

    protected OnFragmentInteractionListener     mListener;
    private View                                rootView;
    protected ArrayList                         musicList;
    protected SpecialSelectableAdapter          musicAdapter;
    protected String                            musicModeType; // Describes the current mode.
    protected AbsListView                       mListView;
    protected ActionMode                        mActionMode;
    protected int playlistSelectionChoice;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AbstractMusicBaseFragment() {
        musicModeType = Constants.UNKNOWN_MODE; //This should be overridden.
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Used to setup the initial view, including calling for the initial data
     * that is shown in the view.
     *
     * TODO, a loading item view after calling the data async.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_music, container, false);

        mListView = (AbsListView) rootView.findViewById(android.R.id.list);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);
        mListView.setEmptyView(rootView.findViewById(android.R.id.empty));
        mListView.setFastScrollEnabled(true);

        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(this);

        // Passing in this as the listener.
        getMusicAsync(this);

        return rootView;
    }

    //runMusicDataQuery

    @Override
    public void onMusicDataSubmitted() {
        if (getActivity() != null) {
            //drawView();
        }
    }

    @Override
    public void onMusicReceivedPrepared(ArrayList result) {
        if (getActivity() != null) {
            musicList = result;
            drawView();
        }
    }

    /**
     * Calls for the data on the device and draws that data on a list
     * view.
     */
    public void drawView() {

        if (musicList.isEmpty()) {
            setEmptyText("Item not found");
        } else {
            // The music adapter will be set by the subclasses.
            // Only initialise the adapter if we haven't already.
            //if (musicAdapter == null) {
                initialiseAdapter(musicList);
                // Set the adapter
            //}
            ((AdapterView<ListAdapter>) mListView).setAdapter(musicAdapter);
        }

        // Either way we should be removing this because the query has returned.
        getActivity().setProgressBarIndeterminateVisibility(false);
    }

    @Override
    public void onResume(){
        super.onResume();

        JukeboxerMainActivity parentActivity = (JukeboxerMainActivity) getActivity();
        if(parentActivity.isMusicBound()) {
            Log.d("Music Controller", "From FRAGMENT", null);
            parentActivity.getMinMusicController().updateControllerView();
        }

        // On resume we'll check whether we have any selections and if we are.


    }

    @Override
    public void onPause(){
        super.onPause();

        //On the pause we'll store the current selection of the hash map.
        //mSelection = musicAdapter.getHashMapSelected();
    }

    /**
     * Builds the query, this will depend on the fragment as it will decide what query
     * should be sent and retrieved.
     */
    abstract public AsyncMusicDBQuery buildAsyncMusicDataQuery();

    /**
     * This will run the method to return the parent (which in this case is MusicTabManagerFragment).
     */
    protected void getMusicAsync(AsyncMusicDBConnector.MusicReceivedListener mListener) {
        // The same for every class.
        getActivity().setProgressBarIndeterminateVisibility(true);
        ((MusicTabManagerFragment) getParentFragment()).getMusicAsync(mListener, buildAsyncMusicDataQuery());
    }

    /**
     *
     * Sets up the adapter with the array list passed as a parameter.
     * This needs to be set in the sub classes so that you can specify
     * the adapter for the type of music.
     *
     * @param musicList - Used to set the relevant adapter.
     */
    abstract public void initialiseAdapter(ArrayList musicList);

    /**
     * Describes the current mode, which should be set in the constructor
     * of the fragment.
     *
     * @return Returns a string that represents which music type that is being returned
     *
     */
    public String getCurrentMusicMode() {
        return musicModeType;
    }

    /**
     * Used to launch the selective music view with the relevant information for the view.
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        //MusicTabManager a = (MusicTabManager) getParentFragment();
        //a.switchFragment();

        AbstractMusic currentMusic = (AbstractMusic) musicList.get(position);

        Intent myIntent = new Intent(getActivity(), SelectiveMusicList.class);
        myIntent.putExtra(Constants.KEY_MUSIC_TYPE_MODE, getCurrentMusicMode());
        myIntent.putExtra(Constants.KEY_MUSIC_ITEM_ID, currentMusic.getId()); //Key, the value
        myIntent.putExtra(Constants.KEY_MUSIC_NAME, currentMusic.getTitle());

        getActivity().startActivity(myIntent);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        getActivity().setProgressBarIndeterminateVisibility(false);
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView)
            ((TextView) emptyView).setText(emptyText);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }

    protected ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.abstract_music_base_context_menu, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_playlist_add:
                    //shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position,
                                          long id, boolean checked) {
        // Here you can do something when items are selected/de-selected,
        // such as update the title in the CAB

        if (checked) {
            musicAdapter.setNewSelection(position, checked);
        } else {
            musicAdapter.removeSelection(position);
        }
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        long[] selectedAudioIDs;

        // Respond to clicks on the actions in the CAB
        mListView.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

        Set<Integer> positions = musicAdapter.getCurrentCheckedPositions();
        selectedAudioIDs = new long[positions.size()];

        int i = 0;
        for (Integer pos : positions) {
            selectedAudioIDs[i] = ((AbstractMusic) musicList.get(i)).getId();
            i++;
        }

        switch (item.getItemId()) {
            case R.id.action_playlist_add_to:
                //Toast.makeText(getActivity(), "Edited entries: " + sb.toString(), Toast.LENGTH_SHORT).show();
                choosePlaylistAndAdd();
                // Choose the playlists available.
                // selectedAudioIDs
                break;

            case R.id.action_playlist_create_new:
                //Toast.makeText(getActivity(), "Deleted entries : " + sb.toString(), Toast.LENGTH_SHORT).show();
                askTitleAndSubmitPlaylist(selectedAudioIDs);
                break;

            case R.id.action_settings:
                musicAdapter.clearSelection();
                mode.finish();
        }

        return false;
    }

    // TODO.
    public void getAFSF() {
        AsyncMusicDBQuery a = new AsyncMusicDBQuery(AsyncMusicDBQuery.GET_ALL_PLAYLIST_MODE);
        //return a;
    }

    public void choosePlaylistAndAdd() {

        int val = 0;

        //Initialize the Alert Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        CharSequence[] array = {"Playlist 1", "Playlist 2", "Playlist 3"};

        // Set the dialog title
        builder.setTitle("Select Playlist")
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected
                .setSingleChoiceItems(array, 1, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        playlistSelectionChoice = which;
                    }
                })

                // Set the action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        // Take my selection, then run the add with the ID we've got.

                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        Dialog dialog = builder.create();
        dialog.show();
    }



    // TODO
    private void addSongsToPlaylist(String titlePlaylist) {
        // No listener but we don't care about the result.
        // buildAsyncMusicDataQuery()
        // AsyncMusicDBQuery a = new AsyncMusicDBQuery(AsyncMusicDBQuery.GET_ALL_SONGS_MODE);

        AsyncMusicDBQuery a = new AsyncMusicDBQuery(AsyncMusicDBQuery.CREATE_NEW_PLAYLIST);
        // TODO, somehow add songs to this.
        a.setPlaylistTitle(titlePlaylist);
        ((MusicTabManagerFragment) getParentFragment()).getMusicAsync(null, a);
        Toast.makeText(getActivity(), "Songs added", Toast.LENGTH_SHORT).show();

    }

    protected void runMusicDataQuery(AsyncMusicDBConnector.MusicReceivedListener mListener,
                                     AsyncMusicDBQuery dBQuery) {
        AsyncMusicDBConnector mDbAsync = new AsyncMusicDBConnector(getActivity(), mListener);
        mDbAsync.execute(dBQuery);
    }

    private void createPlaylist(String titlePlaylist, long[] selectedAudioIDs) {
        AsyncMusicDBQuery a = new AsyncMusicDBQuery(AsyncMusicDBQuery.CREATE_NEW_PLAYLIST_WITH_IDS);
        a.setPlaylistTitle(titlePlaylist);
        a.setPlaylistAudioIDs(selectedAudioIDs);
        runMusicDataQuery(null, a);
        Toast.makeText(getActivity(), "Playlist created.", Toast.LENGTH_SHORT).show();
    }

    /**
     * Will produce a dialog which asks for the users input and then
     * submit the selection for the creation of a playlist. 
     */
    protected void askTitleAndSubmitPlaylist(final long[] selectedAudioIDs) {

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.create_playlist_prompt, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO, does this "casting" work?
                        String playlistTitle = String.valueOf(editText.getText());
                        //TODO, creation of playlist.
                        createPlaylist(playlistTitle, selectedAudioIDs);
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        // Inflate the menu for the CAB
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.abstract_music_base_context_menu, menu);

        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        // Here you can make any necessary updates to the activity when
        // the CAB is removed. By default, selected items are deselected/unchecked.
        musicAdapter.clearSelection();

    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        // Here you can perform updates to the CAB due to
        // an invalidate() request
        return false;
    }

}