package uk.ac.brookes.jvullo11027969.jukebox_api.api_connector;

/**
 * A class to help pass the selected query to {@link JukeboxAPIConnector ()}
 * within the {@link AsyncJukeboxAPIConnector}.
 *
 * You can build a query with this class,
 *
 * Created by jvullo on 01/02/15.
 */
public class JukeboxAPIQuery {

    // These are the modes available.
    public static final int REGISTER_NEW_USER_MODE = 1;
    public static final int AUTHENTICATE_MODE_MODE = 2;
    public static final int GET_USERS_FRIENDS_MODE = 3;
    public static final int GET_USERS_MESSAGES_MODE = 4;
    public static final int GET_USERS_MODE = 5;
    public static final int GET_USERS_NEARBY = 6;
    public static final int GET_JUKEBOXES_NEARBY = 7;
    public static final int GET_STREAMS_NEARBY = 8;

    private String searchQuery = "";
    private int commandType = 0;
    private int id = 0;
    private Object postObject;

    boolean searchQueryExists = false;
    boolean idExists = false;

    /**
     * Pass in something.
     *
     * @param searchQuery
     * @param commandType
     */
    public JukeboxAPIQuery(String searchQuery, int commandType) {
        this.searchQuery = searchQuery;
        this.commandType = commandType;
        searchQueryExists = true;
    }

    public JukeboxAPIQuery(int id, int commandType) {
        this.id = id;
        this.commandType = commandType;
        idExists = true;
    }

    public JukeboxAPIQuery(int commandType) {
        this.commandType = commandType;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public int getCommandType() {
        return commandType;
    }

    public int getId() {
        return id;
    }

    public boolean isIdExists() {
        return idExists;
    }

    public boolean isSearchQueryExists() {
        return searchQueryExists;
    }

    /**
     * Used to get an object that we will covert to post parameters when needed.
     * @return
     */
    public Object getPostObject() {
        return postObject;
    }

    /**
     * Used to store an object that we will covert to post parameters when needed.
     * @param postObject
     */
    public void setPostObject(Object postObject) {
        this.postObject = postObject;
    }
}
