package uk.ac.brookes.jvullo11027969.share;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.main_activity.settings.UserPreferenceActivity;

public class StreamMusicActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream_music);
        setTitle("Streaming... ");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_stream_music, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intentForSettings = new Intent(this, UserPreferenceActivity.class);
                startActivity(intentForSettings);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
