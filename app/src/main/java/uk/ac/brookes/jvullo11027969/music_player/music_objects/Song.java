package uk.ac.brookes.jvullo11027969.music_player.music_objects;

/**
 * Used to represent a song_item_view and create the view for it.
 *
 * Created by jvullo on 10/01/15.
 */
public class Song extends AbstractMusic {

    private String album;
    private long albumId;
    private long playlistId = -1; //i.e the ID value for within the playlist
    // We only need this if we are looking at songs within a playlist.

    // We no longer are storing whether the song has album art.
    // TODO, remove redundant album code.
    private boolean hasArt = false;

    public Song (long id, String title, String artist, String album, long albumId) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.albumId = albumId;
    }

    public Song (long id, String title, String artist, String album, long albumId, long playlistId) {
        this(id, title, artist, album, albumId);
        this.playlistId = playlistId;
    }

    public long getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(long playlistId) {
        this.playlistId = playlistId;
    }

    public String getAlbum() {return album;}

    public long getAlbumId() { return albumId;}

//    public boolean hasAlbumArt() { return hasArt;}

//    public void setHasAlbumArt(boolean newVal) {hasArt = newVal;}


}
