package uk.ac.brookes.jvullo11027969.music_player.music_database;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;


/**
 * Created by jvullo on 01/02/15.
 *
 */
public class AsyncMusicDBConnector extends AsyncTask<Object, Void, ArrayList> {

    AsyncMusicDBQuery           dBQuery;
    MusicDatabaseConnector      mMusicDBConnector;
    MusicReceivedListener       mListener;
    ExtraMusicReceivedListener  mExtraListener;
    MusicDataSubmittedListener  mSubmittedListener;


    public AsyncMusicDBConnector(Context mContext , MusicReceivedListener listener) {
        mMusicDBConnector = new MusicDatabaseConnector(mContext.getContentResolver());
        mListener = listener;
    }

    public AsyncMusicDBConnector(Context mContext , MusicDataSubmittedListener listener) {
        mMusicDBConnector = new MusicDatabaseConnector(mContext.getContentResolver());
        mSubmittedListener = mSubmittedListener;
    }

    /**
     * Sets an extra listener to return the data to the parent fragment {@link uk.ac.brookes.jvullo11027969.music_player.music_interface.MusicTabManagerFragment}
     *
     */
    public void setExtraListener(ExtraMusicReceivedListener mExtraListener) {
        this.mExtraListener = mExtraListener;
    }

    @Override
    protected ArrayList doInBackground(Object... arg0) {

        ArrayList returnValue = null;

        //Get query using the helper
        dBQuery = (AsyncMusicDBQuery) arg0[0];

        switch (dBQuery.getCommandType()) {
            case AsyncMusicDBQuery.GET_ALL_SONGS_MODE:
                returnValue = mMusicDBConnector.getAllSongs();
                break;
            case AsyncMusicDBQuery.GET_ALL_ARTIST_MODE:
                returnValue = mMusicDBConnector.getAllArtists();
                break;
            case AsyncMusicDBQuery.GET_ALL_ALBUM_MODE:
                returnValue = mMusicDBConnector.getAllAlbums();
                break;
            case AsyncMusicDBQuery.GET_ALL_PLAYLIST_MODE:
                returnValue = mMusicDBConnector.getAllPlaylists();
                break;
            case AsyncMusicDBQuery.GET_ARTISTS_RELATED_ALBUMS:
                long id = dBQuery.getId();
                returnValue = mMusicDBConnector.getArtistsRelatedAlbums(id);
                break;
            case AsyncMusicDBQuery.GET_RECENT_ADDED_PLAYLIST:
                returnValue = mMusicDBConnector.getRecentlyAddedPlaylistSongs();
                break;
            case AsyncMusicDBQuery.SEARCH_FOR_ARTISTS:
                returnValue = mMusicDBConnector.searchForArtistsTitle(dBQuery.getSearchQuery());
                break;
            case AsyncMusicDBQuery.SEARCH_FOR_ALBUMS:
                returnValue = mMusicDBConnector.searchForAlbumsTitle(dBQuery.getSearchQuery());
                break;
            case AsyncMusicDBQuery.SEARCH_FOR_ARTIST_ALBUMS:
                returnValue = mMusicDBConnector.searchForArtistAlbums(dBQuery.getSearchQuery());
                break;
            case AsyncMusicDBQuery.SEARCH_FOR_SONG:
                returnValue = mMusicDBConnector.detailedSearchForSong(dBQuery.getSearchQuery());
                break;
            case AsyncMusicDBQuery.SEARCH_FOR_PLAYLIST:
                returnValue = mMusicDBConnector.searchForPlaylistTitle(dBQuery.getSearchQuery());
                break;
            case AsyncMusicDBQuery.GET_PLAYLIST_SONGS:
                returnValue = mMusicDBConnector.getPlaylistSongs(dBQuery.getId());
                break;
            case AsyncMusicDBQuery.GET_ALBUM_OR_ARTIST_SONGS:
                returnValue = mMusicDBConnector.getAlbumOrArtistSongs(dBQuery.getId(), dBQuery.getSearchQuery());
                break;
            case AsyncMusicDBQuery.CREATE_NEW_PLAYLIST:
                mMusicDBConnector.createPlaylistOnly(dBQuery.getPlaylistTitle());
                break;
            case AsyncMusicDBQuery.CREATE_NEW_PLAYLIST_WITH_IDS:
                mMusicDBConnector.createPlaylistWithSongs(dBQuery.getPlaylistTitle(), dBQuery.getPlaylistAudioIDs());
                break;
            case AsyncMusicDBQuery.REMOVE_PLAYLIST_WITH_IDS:
                mMusicDBConnector.removeListOfPlaylists(dBQuery.getPlaylistIDs());
                break;
            default:
                returnValue = null;
                break;
        }

        return returnValue;
    }

    @Override
    protected void onPostExecute(ArrayList result) {

        // We may not have added a listener in some calls.
        if (mListener != null) {
            mListener.onMusicReceivedPrepared(result);
            //mListener.onMusicDataSubmitted();
        }

        // We may not have added a extra listener in some calls as well.
        if (mExtraListener != null) {
            mExtraListener.onMusicReceivedPrepared(result, dBQuery);
        }

        if (mSubmittedListener != null) {
            mSubmittedListener.onMusicDataSubmitted();
        }
    }

    public interface MusicReceivedListener {
        /**
         * Will return with results of a query in an array list.
         * @param result
         */
        public void onMusicReceivedPrepared(ArrayList result);
    }

    public interface ExtraMusicReceivedListener {
        public void onMusicReceivedPrepared(ArrayList result, AsyncMusicDBQuery dBQuery);
    }

    public interface MusicDataSubmittedListener {
        /**
         * Will confirm data submission. For use in items where
         * Removing playlists for example.
         */
        public void onMusicDataSubmitted();
    }
}