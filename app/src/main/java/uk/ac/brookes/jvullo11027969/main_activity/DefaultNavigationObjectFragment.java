package uk.ac.brookes.jvullo11027969.main_activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * This class is used as the default fragment used in the navigation draw
 * temporarily during development of the other pieces of functionality.
 *
 * Created by jvullo on 30/11/14.
 *
 */
public class DefaultNavigationObjectFragment extends Fragment {
    public static final String ARG_OBJECT = "object";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.
        View rootView = inflater.inflate(R.layout.fragment_default_object, container, false);

        // Enable this code to enable the numbers appearing with the object.
        // Was useful during demos, but no longer needed.

//        Bundle args = getArguments();
//        TextView a = ((TextView) rootView.findViewById(R.id.text1));
//
//        if (args != null) {
//            a.setText(Integer.toString(args.getInt(ARG_OBJECT)));
//        } else {
//            a.setText("Null");
//        }


        return rootView;
    }
}