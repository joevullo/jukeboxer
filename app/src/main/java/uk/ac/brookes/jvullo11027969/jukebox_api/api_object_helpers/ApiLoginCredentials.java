package uk.ac.brookes.jvullo11027969.jukebox_api.api_object_helpers;

/**
 * Created by jvullo on 05/03/15.
 */
public class ApiLoginCredentials {

    private String username, password;

    public ApiLoginCredentials(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
