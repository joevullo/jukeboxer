package uk.ac.brookes.jvullo11027969.main_activity.settings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uk.ac.brookes.jvullo11027969.main_activity.R;

/**
 * Written using the facebook developers guide:
 * https://developers.facebook.com/docs/android/login-with-facebook/v2.2#config
 */
public class FacebookFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_register, container, false);

        return view;
    }

}
