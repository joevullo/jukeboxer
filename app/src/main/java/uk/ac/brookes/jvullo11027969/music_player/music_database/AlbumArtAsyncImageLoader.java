package uk.ac.brookes.jvullo11027969.music_player.music_database;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;

import uk.ac.brookes.jvullo11027969.main_activity.R;

/**
 * Created by jvullo on 23/01/15.
 */
public class AlbumArtAsyncImageLoader extends AsyncTask<Object, Long, Bitmap> {

    private View                view;
    private Bitmap              bitmap = null;
    MusicDatabaseConnector      mMDC;
    Context                     mContext;

    @Override
    protected Bitmap doInBackground(Object... parameters) {

        view = (View) parameters[0];
        Long albumId = (Long) parameters[1];
        mContext = (Context) parameters[2];

        mMDC = new MusicDatabaseConnector(mContext.getContentResolver());
        bitmap = mMDC.getAlbumArtwork(albumId);

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        // Got a bitmap and a view.
        if (bitmap != null && view != null) {
            ImageView albumArt = (ImageView) view; //.findViewById(R.id.album_art);
            albumArt.setImageBitmap(bitmap);
            //If we have a view but no bitmap!
        } else if (bitmap == null && view != null) {
            ImageView albumArt = (ImageView) view; //.findViewById(R.id.album_art);
            albumArt.setImageDrawable(mContext.getResources().getDrawable(R.drawable.album_noart));
        } else {
            // This will be expected, we don't need really need to log it.
            //MyDebugger.log(MyDebugger.TAG_ALBUM_IMAGE_LOADER, "either bitmap was or image view.", null, "e");
        }
    }
}
