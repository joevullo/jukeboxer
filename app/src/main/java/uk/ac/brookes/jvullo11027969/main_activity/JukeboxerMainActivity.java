package uk.ac.brookes.jvullo11027969.main_activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import uk.ac.brookes.jvullo11027969.jukebox.JukeboxFragment;
import uk.ac.brookes.jvullo11027969.main_activity.settings.UserPreferenceActivity;
import uk.ac.brookes.jvullo11027969.music_player.music_fragments.AbstractMusicBaseFragment;
import uk.ac.brookes.jvullo11027969.music_player.music_interface.MusicTabManagerFragment;
import uk.ac.brookes.jvullo11027969.share.ShareFragment;
import uk.ac.brookes.jvullo11027969.social.SocialFragment;
import uk.ac.brookes.jvullo11027969.social.SocialTabManagerFragment;

/**
 *
 * This activity is the main activity for the application. It hosts a navigation draw which links
 * the features of the application together.
 *
 */
public class JukeboxerMainActivity extends AbstractMusicPlayerBase implements ShareFragment.OnFragmentInteractionListener ,
        SocialFragment.OnFragmentInteractionListener , JukeboxFragment.OnFragmentInteractionListener,
        AbstractMusicBaseFragment.OnFragmentInteractionListener,
        MusicTabManagerFragment.OnFragmentInteractionListener {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mNavigationTitles;

    // The items for the draw.
    DrawerItem[] drawerItem;
    // These need to match up with the length & order of the navigation array.
    int[] drawImages = {R.drawable.ic_av_play_arrow ,
            R.drawable.ic_av_hearing ,
            R.drawable.ic_av_my_library_music ,
            R.drawable.ic_social_group };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_navigation_draw);

        mTitle = mDrawerTitle = getTitle();
        mNavigationTitles = getResources().getStringArray(R.array.main_navigation_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        //Fill the draw items.
        drawerItem = new DrawerItem[mNavigationTitles.length];
        for (int i = 0; i < drawerItem.length; i++) {
            drawerItem[i] = new DrawerItem(mNavigationTitles[i], drawImages[i]);
        }

        // Set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // set up the drawer's list view with items and click listener

        // This is the simple text version for the drawer.
        // mDrawerList.initialiseAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mNavigationTitles));
        // mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        //This is a custom adapter I built which will display an image with the text for each draw item.
        MyDrawAdapter adapter = new MyDrawAdapter(this, R.layout.custom_drawer_list_item, drawerItem);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app itemIcon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // Magic stuff to set the title and itemIcon when
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app itemIcon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }

        setMusicController();

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_main, menu);
//
////        // Get the SearchView and set the searchable configuration
////        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
////        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
////        // Assumes current activity is the searchable activity
////        searchView.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName(getApplicationContext(), SearchActivity.class)));
////        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
//
//        return super.onCreateOptionsMenu(menu);
//    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_search).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_search:
                Intent activitySearch = new Intent(this, SearchActivity.class);
                startActivity(activitySearch);
                return true;
            case R.id.action_settings:
                Intent intentForSettings = new Intent(this, UserPreferenceActivity.class);
                startActivity(intentForSettings);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {}

    @Override
    public void onFragmentInteraction(String id) {}

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        // Update the main content by replacing fragments
        Fragment fragment;
        //fragment2 = new NewMediaPlayerFragment();

        switch (position) {
            case 0:
                fragment = new MusicTabManagerFragment();
                break;
            case 1:
                fragment = new ShareFragment();
                break;
            case 2:
                fragment = new JukeboxFragment();
                break;
            case 3:
                fragment = new SocialTabManagerFragment();
                break;
            default:
                //Default fragment for a backup.
                fragment = new DefaultNavigationObjectFragment();
                Bundle args2 = new Bundle();
                args2.putInt(DefaultNavigationObjectFragment.ARG_OBJECT, position + 1);
                fragment.setArguments(args2);
                break;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mNavigationTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public ViewGroup getAnchorPointView() {
        return (ViewGroup) findViewById(R.id.content_frame);
    }
}