package uk.ac.brookes.jvullo11027969.music_player.music_fragments;

import android.os.Bundle;
import android.view.ActionMode;
import android.view.HapticFeedbackConstants;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_adapters.PlaylistAdapter;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AsyncMusicDBQuery;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.AbstractMusic;
import uk.ac.brookes.jvullo11027969.utils.Constants;

/**
 * A fragment that implements a view that represents a song by extending the
 * AbstractMusicBaseFragment and overriding the
 *
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class PlaylistsFragment extends AbstractMusicBaseFragment implements AbsListView.OnItemClickListener {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PlaylistsFragment() {
        musicModeType = Constants.PLAYLIST_MODE;
    }
    @Override
    public void initialiseAdapter(ArrayList musicList) {
        musicAdapter = new PlaylistAdapter(getActivity(), musicList);
    }

    @Override
    public AsyncMusicDBQuery buildAsyncMusicDataQuery() {
        AsyncMusicDBQuery a = new AsyncMusicDBQuery(AsyncMusicDBQuery.GET_ALL_PLAYLIST_MODE);
        return a;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Playlists might have been added since onPause, therefore
        // We should update the data from the database when we resume.
        getMusicAsync(this);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.playlist_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    // Called when the action mode is created; startActionMode() was called
    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        // Inflate a menu resource providing context menu items
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.playlist_context_menu, menu);
        return true;
    }

    private void removePlaylists(long[] selectedPlaylistIDs) {
        AsyncMusicDBQuery a = new AsyncMusicDBQuery(AsyncMusicDBQuery.REMOVE_PLAYLIST_WITH_IDS);
        a.setPlaylistIDs(selectedPlaylistIDs);
        runMusicDataQuery(null, a);
        Toast.makeText(getActivity(), "Selected playlists removed.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        long[] selectedPlaylistIDs;

        // Respond to clicks on the actions in the CAB
        mListView.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

        Set<Integer> positions = musicAdapter.getCurrentCheckedPositions();
        selectedPlaylistIDs = new long[positions.size()];

        int i = 0;
        for (Integer pos : positions) {
            selectedPlaylistIDs[i] = ((AbstractMusic) musicList.get(i)).getId();
            i++;
        }

        switch (item.getItemId()) {
            case R.id.action_playlist_remove:
                //Toast.makeText(getActivity(), "Deleted entries : " + sb.toString(), Toast.LENGTH_SHORT).show();
                //askTitleAndSubmitPlaylist(selectedPlaylistIDs);
                removePlaylists(selectedPlaylistIDs);
                musicAdapter.clearSelection();
                mode.finish();
                getMusicAsync(this);
                break;

            case R.id.action_settings:
                musicAdapter.clearSelection();
                mode.finish();
        }
        return false;
    }
}
