package uk.ac.brookes.jvullo11027969.music_player.music_adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AlbumArtAsyncImageLoader;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Artist;
import uk.ac.brookes.jvullo11027969.utils.SpecialSelectableAdapter;

/**
 * Created by jvullo on 10/01/15.
 */
public class ArtistAdapter extends SpecialSelectableAdapter {


    public ArtistAdapter(Context c, ArrayList musicItems) {
        super(c , musicItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout view = (LinearLayout) viewInflater.inflate
                (R.layout.artist_item_view, parent, false);

        TextView artistTitleView = (TextView) view.findViewById(R.id.artist_name);
        TextView numOfArtistsView = (TextView) view.findViewById(R.id.no_of_albums);
        ImageView albumArtView = (ImageView) view.findViewById(R.id.album_art);

        Artist currentArtist = (Artist) dataList.get(position);

        artistTitleView.setText(currentArtist.getTitle());
        numOfArtistsView.setText(""+currentArtist.getNoOfAlbum());

        setSelectedBackgroundView(view, position);

        // TODO, now we are getting the first item, but we should be checking
        // All these albums ID's for the art.
        long a[] = currentArtist.getAlbumIds();

        // TODO, this is not working.
        if (a.length > 0) {
            new AlbumArtAsyncImageLoader().execute(albumArtView, a[0] , c);
        }

        //new AlbumArtImageLoader().execute(albumArtView, currrentAlbum.getId() , c);

        return view;
    }
}