package uk.ac.brookes.jvullo11027969.music_player.music_objects;

/**
 * Used to represent a song_item_view and create the view for it.
 *
 * Created by jvullo on 10/01/15.
 */
public class Playlist extends AbstractMusic {

    public Playlist(long id, String title) {
        this.id = id;
        this.title = title;
    }
}
