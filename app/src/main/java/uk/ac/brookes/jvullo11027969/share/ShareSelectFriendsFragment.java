package uk.ac.brookes.jvullo11027969.share;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.jukebox_api.api_connector.JukeboxAPIQuery;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiUser;
import uk.ac.brookes.jvullo11027969.jukebox_api.reusable_api_fragments.FriendsFragmentBase;

/**
 * This fragment extends the FriendsFragmentBase and implements a CAB selection view.
 * Uses will be able to select multiple friends to start the share functionality with.
 *
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link uk.ac.brookes.jvullo11027969.music_player.music_fragments.AbstractMusicBaseFragment.OnFragmentInteractionListener}
 * interface.
 *
 */
public class ShareSelectFriendsFragment extends FriendsFragmentBase implements AbsListView.OnItemClickListener {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ShareSelectFriendsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setParentActivityTitle("Friends");
        Toast.makeText(getActivity(), "Press and hold to select multiple friends", Toast.LENGTH_LONG);

        return super.onCreateView(inflater, container, savedInstanceState);

//        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
//        mListView.setMultiChoiceModeListener(this);
//
//        return rootView;
    }

    @Override
    public JukeboxAPIQuery buildAsyncAPIDataQuery() {
        return new JukeboxAPIQuery(JukeboxAPIQuery.GET_USERS_FRIENDS_MODE);
    }

    // Consider overriding this in the future.
    //    @Override
    //    public void initialiseAdapter(ArrayList musicList) {
    //        musicAdapter = new AlbumAdapter(getActivity(), musicList);
    //}


    @Override
    public void onAPIDataReceivedPrepared(ArrayList result) {

        apiDataList = result;

        // For now all we want are strings from whatever data we have called.
        simpleListNames = new String[result.size()];
        for (int i = 0; i < simpleListNames.length; i++) {
            simpleListNames[i] = ((ApiUser) result.get(i)).getFullname();
        }

        // This is will store the list and call draw view.
        super.onAPIDataReceivedPrepared(result);
    }

    /**
     * Used to launch the selective music view with the relevant information for the view.
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO, start the share activity.

        //MusicTabManager a = (MusicTabManager) getParentFragment();
        //a.switchFragment();

        //AbstractMusic currentMusic = (AbstractMusic) apiDataList.get(position);

        //Intent myIntent = new Intent(getActivity(), SelectiveMusicList.class);
        //myIntent.putExtra(Constants.KEY_MUSIC_TYPE_MODE, getCurrentMusicMode());
        //myIntent.putExtra(Constants.KEY_MUSIC_ITEM_ID, currentMusic.getId()); //Key, the value
        //myIntent.putExtra(Constants.KEY_MUSIC_NAME, currentMusic.getTitle());


        Intent myIntent = new Intent(getActivity(), StreamMusicActivity.class);
        getActivity().startActivity(myIntent);


    }

}
