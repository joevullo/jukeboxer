package uk.ac.brookes.jvullo11027969.main_activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import uk.ac.brookes.jvullo11027969.main_activity.R;

/**
 *
 * A class that provides option for user settings.
 *
 * @author jvullo
 *
 */
public class UserPreferenceActivity extends ActionBarActivity {

    private UserPreferenceActivity mContext;

    /**
     * On create starts the fragment that most of the heavy lifting.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference_activity);

        setTitle("Preferences");

        mContext = this;

        getFragmentManager().beginTransaction().replace(R.id.container,
                new MyPreferencesFragment()).commit();

        Button a = (Button) findViewById(R.id.registerNewAccount);

        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(mContext, RegisterActivity.class);
                startActivity(myIntent);
            }
        });


        //TODO, view for showing authentication.
        // Authenticated, Attempting or Failed.

        //TODO, disclaimer along the bottom.

    }

    /**
     * Typical onCreate options menu with the setting option page removed
     * as well as refresh and search, since we won't need them.
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.removeItem(R.id.action_settings);
        return true;
    }

    /**
     * Implementing the menu options for help, home and notifications.
     */
    public boolean onOptionsItemSelected(MenuItem item) {

//        int itemId = item.getItemId();
//        if (itemId == R.id.action_search) {
//            //Intent intentForSearch = new Intent(this, AdvertisingActivity.class);
//            //startActivity(intentForAdvertising);
//            finish();
//            return true;
//        } else if (itemId == R.id.action_help) {
//            Intent intentForHelp = new Intent(this, HelpActivity.class);
//            startActivity(intentForHelp);
//            return true;

//            return super.onOptionsItemSelected(item);
//        }

          return super.onOptionsItemSelected(item);
    }

}