package uk.ac.brookes.jvullo11027969.main_activity;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A custom draw adapter which will let me define images and the draw itemIcon's the way I want.
 */
public class MyDrawAdapter extends ArrayAdapter<DrawerItem> {

    Context mContext;
    int layoutResourceId;
    DrawerItem data[] = null;

    public MyDrawAdapter(Context mContext, int layoutResourceId, DrawerItem[] data) {
        super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(layoutResourceId, parent, false);

        ImageView imageViewIcon = (ImageView) listItem.findViewById(R.id.drawer_image_view);
        TextView textViewName = (TextView) listItem.findViewById(R.id.drawer_item_name);

        DrawerItem folder = data[position];

        imageViewIcon.setImageResource(folder.itemIcon);
        textViewName.setText(folder.itemName);

        return listItem;
    }

}
