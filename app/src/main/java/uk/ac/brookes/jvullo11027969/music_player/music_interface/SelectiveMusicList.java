package uk.ac.brookes.jvullo11027969.music_player.music_interface;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

import uk.ac.brookes.jvullo11027969.main_activity.AbstractMusicPlayerBase;
import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_adapters.SongAdapter;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AlbumArtAsyncImageLoader;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AsyncMusicDBConnector;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AsyncMusicDBQuery;
import uk.ac.brookes.jvullo11027969.music_player.music_database.MusicDatabaseConnector;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.AbstractMusic;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Song;
import uk.ac.brookes.jvullo11027969.utils.Constants;
import uk.ac.brookes.jvullo11027969.utils.SpecialSelectableAdapter;
import uk.ac.brookes.jvullo11027969.utils.dynamic_list_view.DynamicListView;

/**
 * Shows a for albums, playlists & artists after clicked on in one of the fragments. s
 * TODO - Grid view implementation.
 */
public class SelectiveMusicList extends AbstractMusicPlayerBase implements AbsListView.OnItemClickListener, AbsListView.MultiChoiceModeListener{

    private long                        musicId;
    private String                      musicMode;
    private DynamicListView             mListView;
    private ArrayList                   musicList;
    protected SpecialSelectableAdapter  musicAdapter;
    protected MusicDatabaseConnector    mMDC;
    private ImageView                   albumArtImage;
    protected ActionMode                mActionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selective_music_list);

        musicMode = getIntent().getExtras().getString(Constants.KEY_MUSIC_TYPE_MODE);
        musicId = getIntent().getExtras().getLong(Constants.KEY_MUSIC_ITEM_ID);
        String activityTitle = getIntent().getExtras().getString(Constants.KEY_MUSIC_NAME);

        setTitle(activityTitle); //Sets the actionbar title.
        TextView otherTitle = (TextView) findViewById(R.id.textView);
        otherTitle.setText(activityTitle);

        // LinearLayout a = findViewById(R.id.title_and_album_art_view);

        albumArtImage = (ImageView) findViewById(R.id.album_art);

        mListView = (DynamicListView) findViewById(android.R.id.list);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);
        mListView.setEmptyView(findViewById(android.R.id.empty));
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(this);

        mMDC = new MusicDatabaseConnector(getContentResolver());

        drawView();

        setMusicController();
    }

    public void drawView() {
        //songView = (ListView) rootView.findViewById(R.id.musicList);
        musicList = getMusicDataList(musicId);

        if (musicList.isEmpty()) {
            setEmptyText("Error: No music found.");
        } else {
            // The music adapter will be set by the subclasses.
            initialiseAdapter(musicList);
            // Set the adapter
            ((AdapterView<ListAdapter>) mListView).setAdapter(musicAdapter);

            new AlbumArtAsyncImageLoader().execute(albumArtImage, ((Song) musicList.get(0)).getAlbumId(), this);
        }
    }

    public void initialiseAdapter(ArrayList musicList) {
        musicAdapter = new SongAdapter(this, musicList);
    }

    public ArrayList getMusicDataList(long id) {

        if (musicMode.equals(Constants.ALBUM_MODE) || musicMode.equals(Constants.ARTIST_MODE) ) {
            return mMDC.getAlbumOrArtistSongs(id,musicMode);
        } else if (musicMode.equals(Constants.PLAYLIST_MODE)) {
            if (id == Constants.RECENT_PLAYLIST_ID)
                return mMDC.getRecentlyAddedPlaylistSongs();
            else
                return mMDC.getPlaylistSongs(id);
        } else {
            return new ArrayList(); //Return an empty list.
        }
    }

    /**
     * The default content for this Activity has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView)
            ((TextView) emptyView).setText(emptyText);
    }

    @Override
    public ViewGroup getAnchorPointView() {
        //return (ViewGroup) findViewById(android.R.id.list);
        return (ViewGroup) findViewById(R.id.content_frame);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_selective_music_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        JukeboxMainActivity a = (JukeboxMainActivity) getActivity();
//        a.setPlaylist(musicList);
//        a.playSong(1);

//        setPlaylist(musicList);

        startMusicPlayer(musicList, position);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        // Inflate the menu for the CAB
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.abstract_music_base_context_menu, menu);

        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        // Here you can make any necessary updates to the activity when
        // the CAB is removed. By default, selected items are deselected/unchecked.
        musicAdapter.clearSelection();

    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        // Here you can perform updates to the CAB due to
        // an invalidate() request
        return false;
    }


    protected ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.abstract_music_base_context_menu, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_playlist_add:
                    //shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position,
                                          long id, boolean checked) {
        // Here you can do something when items are selected/de-selected,
        // such as update the title in the CAB

        if (checked) {
            musicAdapter.setNewSelection(position, checked);
        } else {
            musicAdapter.removeSelection(position);
        }
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        long[] selectedAudioIDs;

        // Respond to clicks on the actions in the CAB
        mListView.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

        Set<Integer> positions = musicAdapter.getCurrentCheckedPositions();
        selectedAudioIDs = new long[positions.size()];

        int i = 0;
        for (Integer pos : positions) {
            selectedAudioIDs[i] = ((AbstractMusic) musicList.get(i)).getId();
            i++;
        }

        switch (item.getItemId()) {
            case R.id.action_playlist_add:
                //Toast.makeText(getActivity(), "Edited entries: " + sb.toString(), Toast.LENGTH_SHORT).show();
                break;

            case R.id.action_playlist_create_new:
                //Toast.makeText(getActivity(), "Deleted entries : " + sb.toString(), Toast.LENGTH_SHORT).show();
                askTitleAndSubmitPlaylist(selectedAudioIDs);
                break;

            case R.id.action_settings:
                musicAdapter.clearSelection();
                mode.finish();
        }
        return false;
    }

    /**
     * Will produce a dialog which asks for the users input and then
     * submit the selection for the creation of a playlist.
     */
    protected void askTitleAndSubmitPlaylist(final long[] selectedAudioIDs) {

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.create_playlist_prompt, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO, does this "casting" work?
                        String playlistTitle = String.valueOf(editText.getText());
                        //TODO, creation of playlist.
                        createPlaylist(playlistTitle, selectedAudioIDs);
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void createPlaylist(String titlePlaylist, long[] selectedAudioIDs) {
        AsyncMusicDBQuery a = new AsyncMusicDBQuery(AsyncMusicDBQuery.CREATE_NEW_PLAYLIST_WITH_IDS);
        a.setPlaylistTitle(titlePlaylist);
        a.setPlaylistAudioIDs(selectedAudioIDs);
        runMusicDataQuery(null, a);
        Toast.makeText(this, "Playlist created.", Toast.LENGTH_SHORT).show();
    }


    protected void runMusicDataQuery(AsyncMusicDBConnector.MusicReceivedListener mListener,
                                     AsyncMusicDBQuery dBQuery) {
        AsyncMusicDBConnector mDbAsync = new AsyncMusicDBConnector(this, mListener);
        mDbAsync.execute(dBQuery);
    }
}