package uk.ac.brookes.jvullo11027969.main_activity;

/**
 * Created by jvullo on 02/12/14.
 *
 * Represents a item on the draw. Stores the item name and the associated image.
 */
public class DrawerItem {

    //Not using getters & setters per android performance recommendations.
    public String itemName;
    public int itemIcon;

    public DrawerItem(String itemName, int icon) {
        super();
        this.itemName = itemName;
        this.itemIcon = icon;
    }
}