package uk.ac.brookes.jvullo11027969.social;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.jukebox_api.api_connector.JukeboxAPIConnector;
import uk.ac.brookes.jvullo11027969.jukebox_api.reusable_api_fragments.FriendsFragmentBase;
import uk.ac.brookes.jvullo11027969.jukebox_api.reusable_api_fragments.NearbyUsersFragmentBase;
import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Artist;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Song;

import static uk.ac.brookes.jvullo11027969.utils.Constants.FRIENDS_TAB_TAG;
import static uk.ac.brookes.jvullo11027969.utils.Constants.MUSIC_NAVIGATION_TABS;
import static uk.ac.brookes.jvullo11027969.utils.Constants.NEARBY_TAB_TAG;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link uk.ac.brookes.jvullo11027969.social.SocialTabManagerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link uk.ac.brookes.jvullo11027969.social.SocialTabManagerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SocialTabManagerFragment extends Fragment {

    private OnFragmentInteractionListener   mListener;
    private FragmentPagerAdapter            adapterViewPager;

    protected ArrayList <Song>               mSongList;
    protected ArrayList <Artist>             mArtistList;

    protected JukeboxAPIConnector            mMDC;
    private String[] tabNames;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TestMusicFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SocialTabManagerFragment newInstance(String param1, String param2) {
        SocialTabManagerFragment fragment = new SocialTabManagerFragment();
        return fragment;
    }

    public SocialTabManagerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabNames = getResources().getStringArray(R.array.social_fragment_tabs_array);
        //mMDC = new MusicDatabaseConnector(getActivity().getContentResolver());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_with_tabs, container, false);
        ViewPager vpPager = (ViewPager) rootView.findViewById(R.id.vpPager);

        adapterViewPager = new SocialPagerAdapter(getChildFragmentManager());
        vpPager.setAdapter(adapterViewPager);

        FragmentTabHost mTabHost = (FragmentTabHost) rootView.findViewById(R.id.tabHost); //Id of tab host

        setupViewPager(vpPager, mTabHost); //Sets up the on page change listener.
        setupTabs(mTabHost, vpPager); //Sets up the tabs.

        return rootView;
    }


//    public void getMusicAsync(AsyncMusicDBConnector.MusicReceivedListener mListener,
//                              AsyncMusicDBQuery dBQuery) {
//
//        // A switch that is used to see if we have called for the data and have stored it in this activity.
//        switch (dBQuery.getCommandType()) {
//
//            case AsyncMusicDBQuery.GET_ALL_SONGS_MODE:
//                if (mSongList != null) {
//                    mListener.onMusicReceivedPrepared(mSongList);
//                    return;
//                }
//                break;
//            case AsyncMusicDBQuery.GET_ALL_ARTIST_MODE:
//                if (mArtistList != null) {
//                    mListener.onMusicReceivedPrepared(mArtistList);
//                    return;
//                }
//                break;
//            case AsyncMusicDBQuery.GET_ALL_ALBUM_MODE:
//                if (mAlbumList != null) {
//                    mListener.onMusicReceivedPrepared(mAlbumList);
//                    return;
//                }
//                break;
//            case AsyncMusicDBQuery.GET_ALL_PLAYLIST_MODE:
//                if (mPlaylistList != null) {
//                    mListener.onMusicReceivedPrepared(mPlaylistList);
//                    return;
//                }
//                break;
//        }

//        // If we have passed the above then we should run the async call.
//        AsyncMusicDBConnector mDbAsync = new AsyncMusicDBConnector(getActivity(), mListener);
//        mDbAsync.setExtraListener(this);
//        mDbAsync.execute(dBQuery);
//    }

    /**
     * Used to setup the tabHost for the activity, which needs to
     * have its functionality linked with the viewpager.
     */
    public void setupTabs(FragmentTabHost mTabHost, final ViewPager vpPager) {

        mTabHost.setup(this.getActivity(), getChildFragmentManager(), android.R.id.tabcontent);

        // Creates the tabs for the music view.

        mTabHost.addTab(
                mTabHost.newTabSpec(MUSIC_NAVIGATION_TABS[0]).setIndicator(tabNames[0], null),
                FriendsFragmentBase.class, null);
        mTabHost.addTab(
                mTabHost.newTabSpec(MUSIC_NAVIGATION_TABS[1]).setIndicator(tabNames[1], null),
                NearbyUsersFragmentBase.class, null);

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {

                int position = 0;

                //Be careful, these will need to be edited if the number or position of tabs change.
                if (tabId == NEARBY_TAB_TAG) {
                    position = 1;
                } else if (tabId == FRIENDS_TAB_TAG) {
                    position = 0;
                }

                vpPager.setCurrentItem(position);

            }
        });


        mTabHost.setCurrentTab(0);

        // For some stupid reason setting the color of the text via a style doesn't work so we need
        // to do it programmatically.
        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            mTabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.apptheme_tab_indicator_holo);

            TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
            tv.setTextColor(Color.parseColor("#ffffff"));
        }
    }

    /**
     * Sets up the on page change listener for this view pager.
     *
     * Also uses the
     *
     * @param vpPager
     */
    public void setupViewPager(ViewPager vpPager, final FragmentTabHost mTabHost) {

        vpPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        // When swiping between pages, select the
                        // corresponding tab.
                        //getSupportActionBar().setSelectedNavigationItem(position);
                        mTabHost.setCurrentTab(position);
                    }
                });
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            // Auto-Gen crap, we don't actually care about this.
            //throw new ClassCastException(activity.toString()
            //        + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
