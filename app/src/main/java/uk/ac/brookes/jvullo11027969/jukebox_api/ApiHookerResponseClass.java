package uk.ac.brookes.jvullo11027969.jukebox_api;

import android.content.Context;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.jukebox_api.api_connector.AsyncJukeboxAPIConnector;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_connector.JukeboxAPIQuery;
import uk.ac.brookes.jvullo11027969.utils.MyDebugger; /**

 * Created by jvullo on 04/03/15.
 *
 * A class that when certain conditions or hooks are passed to it will perform certain applications.
 *
 * TODO This class is poorly named and needs a new one.
 */

public class ApiHookerResponseClass implements AsyncJukeboxAPIConnector.APIDataReceivedListener {

    private Context mContext;

    // These are the conditions that can be passed to this class which we will need to respond to.
    public static final int GENERAL_SETTINGS_CHANGE = 1;
    public static final int NEW_LOGIN_SETTINGS = 3;

    public static final int JUKEBOX_MODE_ENABLED = 2;
    public static final int NEW_LIBRARY_ADDITIONS = 4;
    public static final int CURRENT_PLAYLIST_UPDATES = 5;

    public static final int GEOLOCATION_ENABLED = 6;
    public static final int MUSIC_LIBRARY_ENABLED = 7;
    public static final int RECENT_MUSIC_ENABLED = 8;

    static final public int[] AVAILABLE_CONDITIONS = {};

    public ApiHookerResponseClass(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * Should be passed in
     *
     * @param flag
     */
    public void conditionChange(int flag) {

        // TODO if available conditions.

        switch (flag) {
            case GENERAL_SETTINGS_CHANGE:
                // Any of the general settings have been changed.
                settingsChanged();
                break;
            case NEW_LOGIN_SETTINGS:
                reRegister();
                break;
            case JUKEBOX_MODE_ENABLED:
                updateEnableJukebox();
                break;
            case NEW_LIBRARY_ADDITIONS:
                updateUserMusicLibrary();
                break;
            case CURRENT_PLAYLIST_UPDATES:
                updateUserCurrentPlaylist();
                break;
            case GEOLOCATION_ENABLED:
                updateEnableGeolocation();
                break;
            case MUSIC_LIBRARY_ENABLED:
                updateEnableMusicLibrary();
                break;
            case RECENT_MUSIC_ENABLED:
                updateRecentMusicEnabled();
                break;
        }
    }

    private void updateRecentMusicEnabled() {
        // TODO
    }

    private void updateEnableMusicLibrary() {
        // TODO
    }

    private void updateEnableGeolocation() {
        // TODO
    }

    private void updateUserCurrentPlaylist() {
        // TODO
    }

    private void updateUserMusicLibrary() {
        // TODO
    }

    private void settingsChanged() {
        // TODO
    }

    private void reRegister() {
        // TODO
    }

    private void updateEnableJukebox() {
        // TODO
    }

    private void test() {
        AsyncJukeboxAPIConnector jukeboxAPI = new AsyncJukeboxAPIConnector(mContext, this);
        jukeboxAPI.execute(new JukeboxAPIQuery(JukeboxAPIQuery.GET_USERS_FRIENDS_MODE));
    }

    @Override
    public void onAPIDataReceivedPrepared(ArrayList result) {
        MyDebugger.log(MyDebugger.TAG_API_HOOKER, "Listener has received" + result, null, "v");
    }

}
