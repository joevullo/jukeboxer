package uk.ac.brookes.jvullo11027969.jukebox_api.api_objects;

/**
 * Created by jvullo on 03/03/15.
 */
public class ApiUser {

    private int id; //We may/not have this. 
    private String username = "";
    private String url = "";
    private String email = "";
    private String first_name = "";
    private String last_name = "";
    private String user_status = "", user_friends = "", user_messages = "",
            user_library = "";

    private int location = 0;

    public ApiUser(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public ApiUser(String username, String email, String first_name, String last_name) {
        this.username = username;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public String getFullname() { return (first_name + " " + last_name);}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getUser_friends() {
        return user_friends;
    }

    public void setUser_friends(String user_friends) {
        this.user_friends = user_friends;
    }

    public String getUser_messages() {
        return user_messages;
    }

    public void setUser_messages(String user_messages) {
        this.user_messages = user_messages;
    }

    public String getUser_library() {
        return user_library;
    }

    public void setUser_library(String user_library) {
        this.user_library = user_library;
    }
}
