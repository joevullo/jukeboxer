package uk.ac.brookes.jvullo11027969.jukebox_api.api_objects;

/**
 * Created by jvullo on 03/03/15.
 */
public class ApiJukebox {

    private String username = "";
    private String first_name = "";
    private String last_name = "";
    private String currentLocation = "";

    public ApiJukebox(String username, String first_name, String last_name, String currentLocation) {
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.currentLocation = currentLocation;
    }

    public String getFullname() { return (first_name + " " + last_name);}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }
}
