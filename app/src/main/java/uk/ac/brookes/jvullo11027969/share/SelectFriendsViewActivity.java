package uk.ac.brookes.jvullo11027969.share;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.main_activity.settings.UserPreferenceActivity;

public class SelectFriendsViewActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // This needs to be here to use the progress bar within the fragments
        // Otherwise it doesn't work.
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_view);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_friends_view, menu);
        return true;
    }

//    public void initialiseAdapter(ArrayList musicList) {
//        //musicAdapter = new SongAdapter(getActivity(), musicList);
//    }
//
//    public MusicDBQueryBuilder buildAsyncAPIDataQuery() {
//        MusicDBQueryBuilder a = new MusicDBQueryBuilder(MusicDBQueryBuilder.GET_ALL_SONGS_MODE);
//        return a;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intentForSettings = new Intent(this, UserPreferenceActivity.class);
                startActivity(intentForSettings);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
