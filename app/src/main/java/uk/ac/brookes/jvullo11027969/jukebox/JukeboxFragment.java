package uk.ac.brookes.jvullo11027969.jukebox;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.share.GeolocateNearbyUsersActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link JukeboxFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 *
 */
public class JukeboxFragment extends Fragment {

    private Button startJukebox;
    private Button joinJukebox;
    private OnFragmentInteractionListener mListener;
    private View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_jukebox, container, false);
        setupButtonListeners();
        return rootView;
    }

    public void setupButtonListeners() {
        startJukebox = (Button) rootView.findViewById(R.id.buttonStartNewJukebox);
        joinJukebox = (Button) rootView.findViewById(R.id.buttonJoinJukebox);

        startJukebox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO
                // We need to select some songs.
                // Then pass them onto the Jukeboxer view.
                Intent intent = new Intent(getActivity(), JukeboxActivity.class);
                startActivity(intent);
            }
        });

        joinJukebox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nearbyUsersIntent = new Intent(getActivity(), GeolocateNearbyUsersActivity.class);
                startActivity(nearbyUsersIntent);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
