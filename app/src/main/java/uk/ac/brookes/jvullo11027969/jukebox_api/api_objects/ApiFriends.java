package uk.ac.brookes.jvullo11027969.jukebox_api.api_objects;

/**
 * Created by jvullo on 03/03/15.
 */
public class ApiFriends {

    private String name = "";

    public ApiFriends(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
