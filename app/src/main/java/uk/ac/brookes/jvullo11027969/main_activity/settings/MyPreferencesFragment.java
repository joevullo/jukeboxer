package uk.ac.brookes.jvullo11027969.main_activity.settings;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.utils.MyDebugger;

/**
 * This class handles the preferences for the application.
 *
 * Created by jvullo on 10/01/15.
 *
 * TODO, code cleanup some of these methods are not needed.
 */
public class MyPreferencesFragment extends PreferenceFragment  {

    private MyDebugger debugger;
    SharedPreferences userPrefs;
    private OnSharedPreferenceChangeListener listener;
    private boolean havePushCriticalSettingsBeenChanged;
    private boolean preferencesHaveBeenEdited;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        userPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        debugger = new MyDebugger();

        havePushCriticalSettingsBeenChanged = false;
        preferencesHaveBeenEdited = false;

        listener = new SharedPreferences.OnSharedPreferenceChangeListener() {

            /**
             * This will run after the preference has been saved!
             */
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {

            }
        };
    }

    /**
     * On resume will register the OnSharedPreferenceChangeListener
     */
    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(listener);

    }

    /**
     * Unregisters from the shared preference change listener, also edits the shared preferences
     * if the setting of the application have changed.
     *
     * This application may log several times extra then intended since onPause may run but the activity
     * might not close as a result these settings will not change back to normal.
     */
    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(listener);

        Editor prefEditor = userPrefs.edit();
    }
}