package uk.ac.brookes.jvullo11027969.jukebox_api.api_objects_adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiStream;
import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.utils.SpecialSelectableAdapter;

/**
 * Created by jvullo on 10/01/15.
 */
public class ApiStreamAdapter extends SpecialSelectableAdapter {

    public ApiStreamAdapter(Context c, ArrayList musicItems) {
        super(c , musicItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout view = (LinearLayout) viewInflater.inflate
                (R.layout.api_user_item_view, parent, false);

        TextView usersNameText = (TextView) view.findViewById(R.id.users_name);
        TextView distanceText = (TextView) view.findViewById(R.id.usersDistance);
        ImageView userIcons = (ImageView) view.findViewById(R.id.api_users_picture);

        ApiStream currrentUser = (ApiStream) dataList.get(position);
        usersNameText.setText(currrentUser.getFullname());

        // For now we are hiding distance.
        distanceText.setText(currrentUser.getFullname());
        distanceText.setVisibility(View.GONE);

        //if (currrentAlbum.hasAlbumArt())
        //    albumArtView.setImageBitmap(currrentAlbum.getAlbumArtBitmap());

        // Loads the album art asynchronous TODO: Loading of the users icon.
        // new AlbumArtAsyncImageLoader().execute(albumArtView, currrentAlbum.getId() , c);

        setSelectedBackgroundView(view, position);

        return view;
    }
}