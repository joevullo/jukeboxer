package uk.ac.brookes.jvullo11027969.jukebox_api.api_objects;

import uk.ac.brookes.jvullo11027969.music_player.music_objects.Song;

/**
 * Created by jvullo on 03/03/15.
 *
 * Extends the
 */
public class ApiSong extends Song {

    public ApiSong(long id, String title, String artist, String album, long albumId) {
        super(id, title, artist, album, albumId);
    }

}
