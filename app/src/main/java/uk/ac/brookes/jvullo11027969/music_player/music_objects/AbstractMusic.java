package uk.ac.brookes.jvullo11027969.music_player.music_objects;

import android.graphics.Bitmap;

/**
 * Used to represent a song_item_view and create the view for it.
 *
 * Created by jvullo on 10/01/15.
 */
abstract public class AbstractMusic {

    // Every item will have at least these attributes.
    protected long id;
    protected String title;
    protected String artist;

    //public Music() {}

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

}
