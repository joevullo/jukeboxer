package uk.ac.brookes.jvullo11027969.share;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.jukebox_api.api_connector.JukeboxAPIQuery;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiStream;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects_adapters.ApiStreamAdapter;
import uk.ac.brookes.jvullo11027969.jukebox_api.reusable_api_fragments.NearbyUsersFragmentBase;

/**
 * This
 *
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link uk.ac.brookes.jvullo11027969.music_player.music_fragments.AbstractMusicBaseFragment.OnFragmentInteractionListener}
 * interface.
 *
 */
public class ShareNearbyUsersFragment extends NearbyUsersFragmentBase
        implements AbsListView.OnItemClickListener {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ShareNearbyUsersFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setParentActivityTitle("Nearby Jukeboxes");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public JukeboxAPIQuery buildAsyncAPIDataQuery() {
        return new JukeboxAPIQuery(JukeboxAPIQuery.GET_STREAMS_NEARBY);
    }

    @Override
    public void onAPIDataReceivedPrepared(ArrayList result) {

        if (result != null) {

            apiDataList = result;
            // For now all we want are strings from whatever data we have called.
            simpleListNames = new String[result.size()];
            for (int i = 0; i < simpleListNames.length; i++) {
                simpleListNames[i] = ((ApiStream) result.get(i)).getFullname();
            }
        }
        // This is will store the list and call draw view.
        super.onAPIDataReceivedPrepared(result);

    }

    @Override
    public void initialiseAdapter(ArrayList apiDataList) {
        dataAdapter = new ApiStreamAdapter(getActivity(), apiDataList);
    }

    /**
     * Used to launch the selective music view with the relevant information for the view.
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO, start the share activity.

        //MusicTabManager a = (MusicTabManager) getParentFragment();
        //a.switchFragment();

        //AbstractMusic currentMusic = (AbstractMusic) apiDataList.get(position);

        //Intent myIntent = new Intent(getActivity(), SelectiveMusicList.class);
        //myIntent.putExtra(Constants.KEY_MUSIC_TYPE_MODE, getCurrentMusicMode());
        //myIntent.putExtra(Constants.KEY_MUSIC_ITEM_ID, currentMusic.getId()); //Key, the value
        //myIntent.putExtra(Constants.KEY_MUSIC_NAME, currentMusic.getTitle());


        Intent myIntent = new Intent(getActivity(), StreamMusicActivity.class);
        getActivity().startActivity(myIntent);
    }

}
