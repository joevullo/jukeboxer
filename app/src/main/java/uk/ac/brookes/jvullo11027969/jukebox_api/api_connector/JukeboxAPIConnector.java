package uk.ac.brookes.jvullo11027969.jukebox_api.api_connector;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiFriends;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiJukebox;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiMessages;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiStream;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiUser;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiUserMusicLibrary;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiUserStatus;
import uk.ac.brookes.jvullo11027969.utils.MyDebugger;

/**
 * A class that
 *
 * Created by jvullo on 22/01/15.
 *
 */
public class JukeboxAPIConnector {

    public final boolean ENABLE_TEST_DATA = true;
    public static final String JUKEBOX_SERVER_ADDRESS  = "http://192.168.0.30:8000/";
    private static Context mContext;

    private String token     = "";


    // The following variables are used in testing the application while the json methods
    // Have not been finished.
    ArrayList<ApiFriends> friendsTestData = new ArrayList<ApiFriends>();
    ArrayList<ApiUser> usersTestData = new ArrayList<ApiUser>();
    ArrayList<ApiJukebox> jukeboxTestData = new ArrayList<ApiJukebox>();
    ArrayList<ApiStream> streamsTestData = new ArrayList<ApiStream>();

    String[] first_names_test_data = {"James", "Joe", "Gemma", "John", "William", "Harry", "David", "Faye", "Cem", "Jeff" };
    String[] last_names_test_data = {"Carrow", "Potter", "Amacus", "Opeor", "Warning", "Bang", "Alecto", "Riddle", "Mitchel", "Bellows" };

    Gson gson;

    public JukeboxAPIConnector(Context c) {
        mContext = c; 
        setupTestingData();
        GsonBuilder builder =   new GsonBuilder();
        gson = builder.create();
    }

    // Force the current token to be a particular value; useful when
    // instantiating this class when a token has already been obtained
    // elsewhere, to save an extra login call (assuming it's still valid)
    public void setToken(String newToken) {
        token = newToken;
    }

    /**
     * This method
     */
    public void setupTestingData() {
        // Adding in some data.
        for (int i = 0; i < 10; i++) {
            friendsTestData.add(new ApiFriends("User " + i));
            usersTestData.add(new ApiUser("User " + i, "User"+i+"@"+"example.com", first_names_test_data[i], last_names_test_data[i]));
            jukeboxTestData.add(new ApiJukebox("User " + i, first_names_test_data[i], last_names_test_data[i], ""));
            streamsTestData.add(new ApiStream("User " + i, first_names_test_data[i], last_names_test_data[i], ""));
        }

    }

    public void registerNewUser() {
        // TODO
    }

    public void authenticate() {
        // TODO
    }

    public ArrayList<ApiStream> getNearbyStreams() {
        return streamsTestData;
    }

    public ArrayList<ApiJukebox> getNearbyJukebox() {
        return jukeboxTestData;
    }

    public ArrayList<ApiUser> getUsersFriends() {
        return usersTestData;
    }

    public ArrayList<ApiUser> getUsersNearby() {
        if(ENABLE_TEST_DATA) return usersTestData;
        // TODO
        return null;
    }

    public ArrayList<ApiMessages> getMessages() {
        String getUserAddress = JUKEBOX_SERVER_ADDRESS + "message/";
        String a = HTTP_GET(getUserAddress);
        ArrayList<ApiMessages> list = gson.fromJson(a, ArrayList.class);
        return list;
    }

    public ArrayList<ApiMessages> getUsersMessages(int id) {
        String getUserAddress = JUKEBOX_SERVER_ADDRESS + "messages/" + id;
        String a = HTTP_GET(getUserAddress);
        ArrayList<ApiMessages> list = gson.fromJson(a, ArrayList.class);
        return list;
    }

    public ArrayList<ApiUserMusicLibrary> getUsersLibrary() {
        String getUserAddress = JUKEBOX_SERVER_ADDRESS + "status/";
        String a = HTTP_GET(getUserAddress);
        ArrayList<ApiUserMusicLibrary> list = gson.fromJson(a, ArrayList.class);
        return list;
    }


    public ArrayList<ApiUserMusicLibrary> getUserLibrary(int id) {
        String getUserAddress = JUKEBOX_SERVER_ADDRESS + "status/" + id;
        String a = HTTP_GET(getUserAddress);
        ArrayList<ApiUserMusicLibrary> list = gson.fromJson(a, ArrayList.class);
        return list;
    }

    public ArrayList<ApiUserStatus> getUsersStatus() {
        String getUserAddress = JUKEBOX_SERVER_ADDRESS + "status/";
        String a = HTTP_GET(getUserAddress);
        ArrayList<ApiUserStatus> statuses = gson.fromJson(a, ArrayList.class);
        return statuses;
    }

    public ArrayList<ApiUserStatus> getUserStatus(int id) {
        String getUserAddress = JUKEBOX_SERVER_ADDRESS + "status/" + id;
        String a = HTTP_GET(getUserAddress);
        ArrayList<ApiUserStatus> statuses = gson.fromJson(a, ArrayList.class);
        return statuses;
    }

    public ArrayList<ApiJukebox> getUserJukebox(int id) {
        String getUserAddress = JUKEBOX_SERVER_ADDRESS + "jukebox/" + id;
        String a = HTTP_GET(getUserAddress);
        ArrayList<ApiJukebox> list = gson.fromJson(a, ArrayList.class);
        return list;
    }

    public ArrayList<ApiJukebox> getJukeboxs() {
        String getUserAddress = JUKEBOX_SERVER_ADDRESS + "jukebox/";
        String a = HTTP_GET(getUserAddress);
        ArrayList<ApiJukebox> list = gson.fromJson(a, ArrayList.class);
        return list;
    }

    public ArrayList<ApiUser> getUser(int id) {
        String getUserAddress = JUKEBOX_SERVER_ADDRESS + "users/" + id;
        String a = HTTP_GET(getUserAddress);
        ArrayList<ApiUser> users = gson.fromJson(a, ArrayList.class);
        return users;
    }

    public ArrayList<ApiUser> getUsers() {
        if(ENABLE_TEST_DATA) return usersTestData;

        String getUserAddress = JUKEBOX_SERVER_ADDRESS + "users/";
        String a = HTTP_GET(getUserAddress);
        ArrayList<ApiUser> users = gson.fromJson(a, ArrayList.class);
        return users;
    }

    // This code can produce a null result, be careful.
    private HttpResponse HTTP_POST(JSONObject postMessage, String address) {

        preNetworkChecks();

        int TIMEOUT_MILLISEC = 10000;  // = 10 seconds
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
        HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
        HttpClient client = new DefaultHttpClient(httpParams);

        HttpPost request = new HttpPost(address);

        // Only add the post data if we need.
        if (postMessage != null) {
            // This is within a try because we may have an unsupported coding expection.
            try {
                request.setEntity(new ByteArrayEntity(postMessage.toString().getBytes("UTF8")));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null; //Also return from the function because we don't want to try posting anything.
            }
        }

        // Trying here because we may not get a connection for some reason.
        try {
            HttpResponse response = client.execute(request);
            return response;
            // TODO, what are we doing with the response?
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void LOGIN_TEST() {
        //JSONParser jParser = new JSONParser();
        String username = "TEST";
        String password = "TEST";
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("p", password));
        params.add(new BasicNameValuePair("u", username));
        ResourceBundle json = null;
        //Object jParser;
        //ResourceBundle json = jParser.makeHttpRequest(JUKEBOX_SERVER_ADDRESS + "login", "GET", params);
        String s = null;
        s = json.getString("info");
        if (s.equals("SUCCESS")) {}
    }

    public void updateMusicLibrary() {
        // TODO
    }

    public void updatePlaylist() {
        // TODO
    }

    public void suggestSongsToUser() {
        // TODO
    }

    public void addUserAsFriend() {
        // TODO
    }

    public static String HTTP_GET(String url){

        preNetworkChecks();

        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make HTTP_GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            httpResponse.getStatusLine().getStatusCode();

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    // TODO, finish this.
    public static boolean testConnection() {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make HTTP_GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(JUKEBOX_SERVER_ADDRESS));

            httpResponse.getStatusLine().getStatusCode();

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return true;
    }

    public static void preNetworkChecks() {
        if (!isNetworkAvailable() || !testConnection()) {
            MyDebugger.log(MyDebugger.TAG_JUKEBOX_API_CONNECTOR, "", null, "v");
        }
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

}

//    //TODO, remove this test.
//    private void testGsonLibrary() {
//        ApiUser u = new ApiUser();
//        u.id = 0;
//        u.username = "Album 1";
//        GsonBuilder builder = new GsonBuilder();
//        Gson gson = builder.create();
//        String test = gson.toJson(u);
//
//        ApiUser animal1 = gson.fromJson(test, ApiUser.class);
//        animal1.id = 1;
//    }