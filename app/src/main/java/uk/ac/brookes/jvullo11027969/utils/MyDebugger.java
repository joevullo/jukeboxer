package uk.ac.brookes.jvullo11027969.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import uk.ac.brookes.jvullo11027969.main_activity.BuildConfig;

/**
 * This class allows users to static pass logging data to it.
 * The usefulness here is that there is a boolean to turn of logging during demos &
 * an only tags that exist in the allow tags string array will display.
 *
 * @author jvullo
 */
public final class MyDebugger {

    // Global debugging on/off
    public static final boolean     ENABLE_GLOBAL_DEBUG = true;

    //Enables all tags for debugging.
    public static final boolean     ENABLE_ALL_TAGS = false;
    public static final boolean     SHOW_ERRORS_ALWAYS = true;

    // Tags for activities to use.
    public final static String      TAG_DEBUGGER = "myDebugger";
    public final static String      TAG_MUSIC_PLAYER = "musicPlayer";
    public final static String      TAG_SOCIAL = "social";
    public final static String      TAG_JUKEBOX = "jukebox";
    public final static String      TAG_SHARE = "share";
    public final static String      TAG_MUSIC_SERVICE = "Music Service";
    public final static String      TAG_MUSIC_DATABASE_CONNECTOR = "Music Data Connection";
    public final static String      TAG_MIN_MUSIC_CONTROLLER = "Music Controller";
    public final static String      TAG_ALBUM_IMAGE_LOADER = "Album Image Loader";
    public final static String      TAG_JUKEBOX_API_CONNECTOR = "jukebox api connector tag";
    public final static String      TAG_API_HOOKER = "API Hooker";
    public final static String      TAG_FRIENDS_FRAGMENT = "Friends Fragment";
    public final static String      TAG_NEARBY_FRAGMENT = "Nearby Fragment";

    // This array will control what tags are shown, add tags into this array to enable the
    // logging for those tags.
    private final static String[] ALLOWED_TAGS = {"TEST" , TAG_MUSIC_DATABASE_CONNECTOR, TAG_MIN_MUSIC_CONTROLLER, TAG_MUSIC_SERVICE}; /*Empty means no tags allowed!*/


    /**
     * Checks the ALLOWED_TAGS array to see if the tag passed to the tagToCheck parameter
     * is allowed to be logged by for looping and testing whether the tag has been added to that
     * array.
     *
     * However if the global variable ENABLE_ALL_TAGS is enabled then all tags are allowed.
     *
     * @param tagToCheck - The tag to check against the array.
     * @return either true of false is the tag is allowed or not.
     */
    public static boolean isTagAllowed(String tagToCheck) {

        if (ENABLE_ALL_TAGS) {
            return true;
        }

        for (int i = 0; i < ALLOWED_TAGS.length; i++) {
            if (tagToCheck.equals(ALLOWED_TAGS[i])) {
                return true;
            }
        }

        //If you reached here than the tag is obviously not in the array.
        return false;
    }

    /**
     * This is the method for printing debug to the log. Like debug toast it has a flag that it tests
     * (Constants.DEBUG) before it continues.
     *
     * Setting this up means that I can leave the debugging in between builds.
     *
     * Also added an if that should stop the logging be accidently left in for non debug build.
     *
     * Also added an allow tags array up the top, this method will allow log if the tag passed to it is within
     * the allowed tags section of the application, unless the log is an error message, in which case it is logged regardless
     * of tag.
     *
     * @param tag         - The tag passed in to the debug.
     * @param debugString - The string to debug.
     * @param e      the exception to the log.
     * @param debugLevel  - The level of the debug to log. For example W for Warning. e v i w d are all options.
     */
    public static void log(String tag, String debugString, Exception e, String debugLevel) {

        // If the build config is not a debug build than there should be no logging on the device.
        if (!BuildConfig.DEBUG) {
            return;
        }

        // or if global debugging is off then no logging should be performed.
        if (!ENABLE_GLOBAL_DEBUG) {
            return;
        } else {

            if (isTagAllowed(TAG_DEBUGGER)) {
                Log.i(TAG_DEBUGGER, " Global debugging is on");
            }
        }

        // or if the tag is not allowed, quit early.
        if (!isTagAllowed(tag) && !debugLevel.equalsIgnoreCase("e")) {

            if (isTagAllowed(TAG_DEBUGGER))
                Log.i(TAG_DEBUGGER, tag + " Tag is not allowed ");

            return;
        } else {
            if (isTagAllowed(TAG_DEBUGGER))
                Log.i(TAG_DEBUGGER, tag + " Tag is allowed ");
        }


        if (e == null) {

            if (debugLevel.equalsIgnoreCase("d")) {
                Log.d(tag, debugString, e);
            } else if (debugLevel.equalsIgnoreCase("w")) {
                Log.w(tag, debugString, e);
            } else if (debugLevel.equalsIgnoreCase("e")) {
                Log.e(tag, debugString, e);
            } else if (debugLevel.equalsIgnoreCase("v")) {
                Log.v(tag, debugString, e);
            } else if (debugLevel.equalsIgnoreCase("i")) {
                Log.i(tag, debugString, e);
            } else if (debugLevel.equalsIgnoreCase("wtf")) {
                Log.wtf(tag, debugString, e);
            } else {
                Log.e(TAG_DEBUGGER, "An error has occured with the debuging logger. Have you passed in the correct debug level?");
            }
        } else {
            if (debugLevel.equalsIgnoreCase("d")) {
                Log.d(tag, debugString);
            } else if (debugLevel.equalsIgnoreCase("w")) {
                Log.w(tag, debugString);
            } else if (debugLevel.equalsIgnoreCase("e")) {
                Log.e(tag, debugString);
            } else if (debugLevel.equalsIgnoreCase("v")) {
                Log.v(tag, debugString);
            } else if (debugLevel.equalsIgnoreCase("i")) {
                Log.i(tag, debugString);
            } else if (debugLevel.equalsIgnoreCase("wtf")) {
                Log.wtf(tag, debugString);
            } else {
                Log.e(TAG_DEBUGGER, "An error has occured with the debuging logger. Have you passed in the correct debug level?");
            }
        }
    }

    /**
     * A debug toast which tests for the debugging flag before displaying the toast.
     * Setting this up means that I can leave the debugging in between builds.
     *  
     * @param debugString        - The string to be debugged.
     * @param newActivityContext - The context that you want the toast to use.
     */
    public static void debuggerToast(String debugString, Context newActivityContext) {

        // If the build config is not a debug build than there should be no logging on the device.
        if (!BuildConfig.DEBUG) {
            return;
        }

        if (!ENABLE_GLOBAL_DEBUG)
            return;

        if (newActivityContext != null)
            Toast.makeText(newActivityContext, debugString, Toast.LENGTH_SHORT).show();
    }
}