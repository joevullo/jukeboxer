package uk.ac.brookes.jvullo11027969.jukebox_api.reusable_api_fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.jukebox_api.api_connector.JukeboxAPIQuery;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiUser;

/**
 * A fragment shows data relating to the users friends by calling the database.
 *
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link uk.ac.brookes.jvullo11027969.music_player.music_fragments.AbstractMusicBaseFragment.OnFragmentInteractionListener}
 * interface.
 *
 *
 */
public class FriendsFragmentBase extends AbstractSimpleAPIListBaseFragment
        implements AbsListView.OnItemClickListener {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FriendsFragmentBase() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setParentActivityTitle("Friends");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public JukeboxAPIQuery buildAsyncAPIDataQuery() {
        return new JukeboxAPIQuery(JukeboxAPIQuery.GET_USERS_FRIENDS_MODE);
    }

    // Consider overriding this in the future.
    //    @Override
    //    public void initialiseAdapter(ArrayList musicList) {
    //        musicAdapter = new AlbumAdapter(getActivity(), musicList);
    //}


    @Override
    public void onAPIDataReceivedPrepared(ArrayList result) {

        apiDataList = result;

        // For now all we want are strings from whatever data we have called.
        simpleListNames = new String[result.size()];
        for (int i = 0; i < simpleListNames.length; i++) {
            simpleListNames[i] = ((ApiUser) result.get(i)).getFullname();
        }

        // This is will store the list and call draw view.
        super.onAPIDataReceivedPrepared(result);
    }

}
