/*
* Copyright (C) 2006 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package uk.ac.brookes.jvullo11027969.music_player.music_controls;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import uk.ac.brookes.jvullo11027969.main_activity.DetailedMusicPlayerActivity;
import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AlbumArtAsyncImageLoader;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Song;
import uk.ac.brookes.jvullo11027969.utils.MyDebugger;

/**
* A view containing controls for a MediaPlayer. Typically contains the
* buttons like "Play/Pause", "Rewind", "Fast Forward" and a progress
* slider. It takes care of synchronizing the controls with the state
* of the MediaPlayer.
* <p>
* The way to use this class is to instantiate it programatically.
* The MediaController will create a default set of controls
* and put them in a window floating above your application. Specifically,
* the controls will float above the view specified with setAnchorView().
* The window will disappear if left idle for three seconds and reappear
* when the user touches the anchor view.
* <p>
* Functions like show() and hide() have no effect when MediaController
* is created in an xml layout.
*
* MediaController will hide and
* show the buttons according to these rules:
* <ul>
* <li> The "previous" and "next" buttons are hidden until setPrevNextListeners()
*   has been called
* <li> The "previous" and "next" buttons are visible but disabled if
*   setPrevNextListeners() was called with null listeners
* <li> The "rewind" and "fastforward" buttons are shown unless requested
*   otherwise by using the MediaController(Context, boolean) constructor
*   with the boolean set to false
* </ul>
 *
*/
public class MinimalMusicControllerView extends FrameLayout {

    private MinMediaPlayerControl mPlayer;
    private Context             mContext;
    private ViewGroup           mAnchor;
    private View                mRoot;
    private boolean             mShowing;
    private static final int    sDefaultTimeout = 0;
    private static final int    FADE_OUT = 1;
    private static final int    SHOW_PROGRESS = 2;
    private Handler             mHandler = new MessageHandler(this);

    //View variables
    private ImageButton         mPauseButton; //Also the play button.
    private ImageView           mAlbumArt;
    private TextView            mSongTitle;
    private TextView            mArtistTitle;

    public MinimalMusicControllerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mRoot = null;
        mContext = context;
    }

    public MinimalMusicControllerView(Context context, boolean useFastForward) {
        super(context);
        mContext = context;
    }

    public MinimalMusicControllerView(Context context) {
        this(context, true);
    }

    @Override
    public void onFinishInflate() {
        if (mRoot != null)
            initControllerView(mRoot);
    }

    public void setMediaPlayer(MinMediaPlayerControl player) {
        mPlayer = player;
        updatePausePlay();
    }

    /**
     * Set the view that acts as the anchor for the control view.
     * This can for example be a VideoView, or your Activity's main view.
     * @param view The view to which to anchor the controller when it is visible.
     */
    public void setAnchorView(ViewGroup view) {
        mAnchor = view;

        /*LayoutParams frameParams = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );*/

        LayoutParams frameParams = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );

        removeAllViews();
        View v = makeControllerView();
        addView(v, frameParams);
    }

    /**
     * Create the view that holds the widgets that control playback.
     * Derived classes can override this to create their own.
     * @return The controller view.
     * @hide This doesn't work as advertised
     */
    protected View makeControllerView() {
        LayoutInflater inflate = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRoot = inflate.inflate(R.layout.minimal_media_controller, null);

        initControllerView(mRoot);

        return mRoot;
    }

    private void initControllerView(View v) {
        mPauseButton = (ImageButton) v.findViewById(R.id.pause);
        if (mPauseButton != null) {
            mPauseButton.requestFocus();
            mPauseButton.setOnClickListener(mPauseListener);
        }

        mAlbumArt = (ImageView) v.findViewById(R.id.album_art);
        mSongTitle = (TextView) v.findViewById(R.id.min_song_title);
        mArtistTitle = (TextView) v.findViewById(R.id.min_artist_title);

        // Sets an onClick listener.
        v.setOnClickListener(mMinControllerListener);

    }

    /**
     * Show the controller on screen. It will go away
     * automatically after 3 seconds of inactivity.
     */
    public void show() {
        show(sDefaultTimeout);
    }

    /**
     * Disable pause or seek buttons if the stream cannot be paused or seeked.
     * This requires the control interface to be a MediaPlayerControlExt
     */
    private void disableUnsupportedButtons() {
        if (mPlayer == null) {
            return;
        }

        try {
            if (mPauseButton != null && !mPlayer.canPause()) {
                mPauseButton.setEnabled(false);
            }
        } catch (IncompatibleClassChangeError ex) {
            // We were given an old version of the interface, that doesn't have
            // the canPause/canSeekXYZ methods. This is OK, it just means we
            // assume the media can be paused and seeked, and so we don't disable
            // the buttons.
        }
    }

    /**
     * Updates the song title/artist/album & album art view.
     */
    public void updateSongInfo() {

        Song s = mPlayer.getCurrentSong();


        if (s == null) {
            // We don't need to log this, it can happen a lot.
            // MyDebugger.log(MyDebugger.TAG_MIN_MUSIC_CONTROLLER, "Song used to " +
            //        "draw \"UpdateSongInfo\" was null.", null, "e");
            return;
        }

        if (mAlbumArt != null && mSongTitle != null && mArtistTitle != null) {

            MyDebugger.log(MyDebugger.TAG_MIN_MUSIC_CONTROLLER, "Controller view updated with " + s.getTitle()
                    + "'s info.", null, "v");

            mSongTitle.setText(s.getTitle());
            mArtistTitle.setText(s.getArtist());
            new AlbumArtAsyncImageLoader().execute(mAlbumArt, s.getAlbumId(), mContext);

        } else {
            //MyDebugger.log(MyDebugger.TAG_MIN_MUSIC_CONTROLLER, "One of the views used " +
            //        " in \"UpdateSongInfo\" was null.", null, "v");
            return;
        }
    }

    /**
     * Called to refresh the view's content once the song has been changed.
     * Should be called whenever a song is played, or playNext or playPrevious is called.
     *
     * Be careful, this shares some simularites with the "show" method, which is
     * traditionally used to "show" the media player but also updates the view as
     * a result.
     *
     */
    public void updateControllerView() {
        updatePausePlay();
        updateSongInfo();
    }

    public void adjustFrameLayoutForController() {
          // TODO

        //LayoutParams params = (LayoutParams) mAnchor.getLayoutParams();
        ViewGroup.LayoutParams layoutParams = mAnchor.getLayoutParams();
        layoutParams.height = 100;
        mAnchor.setLayoutParams(layoutParams);
//        // Changes the height and width to the specified *pixels*
//        params.height = 100;
//        params.width = 100;
    }

    /**
     * Show the controller on screen. It will go away
     * automatically after 'timeout' milliseconds of inactivity.
     * @param timeout The timeout in milliseconds. Use 0 to show
     * the controller until hide() is called.
     */
    public void show(int timeout) {
        if (!mShowing && mAnchor != null) {
            if (mPauseButton != null) {
                mPauseButton.requestFocus();
            }

            disableUnsupportedButtons();

            LayoutParams tlp = new LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM
            );

            //adjustFrameLayoutForController(); // TODO: this hasn't been implemented, but the idea
            // Is to shorten the frame size so that the controller doesn't overlap anything important.

            mAnchor.addView(this, tlp);
            mShowing = true;
        }
        updatePausePlay();

        // cause the progress bar to be updated even if mShowing
        // was already true.  This happens, for example, if we're
        // paused with the progress bar showing the user hits play.
        mHandler.sendEmptyMessage(SHOW_PROGRESS);

        Message msg = mHandler.obtainMessage(FADE_OUT);
        if (timeout != 0) {
            mHandler.removeMessages(FADE_OUT);
            mHandler.sendMessageDelayed(msg, timeout);
        }
    }

    public boolean isShowing() {
        return mShowing;
    }

    /**
     * Remove the controller from the screen.
     */
    public void hide() {
        if (mAnchor == null) {
            return;
        }

        try {
            mAnchor.removeView(this);
            mHandler.removeMessages(SHOW_PROGRESS);
        } catch (IllegalArgumentException ex) {
            Log.w("MediaController", "already removed");
        }
        mShowing = false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        show(sDefaultTimeout);
        return true;
    }

    @Override
    public boolean onTrackballEvent(MotionEvent ev) {
        show(sDefaultTimeout);
        return false;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (mPlayer == null) {
            return true;
        }

        int keyCode = event.getKeyCode();
        final boolean uniqueDown = event.getRepeatCount() == 0
                && event.getAction() == KeyEvent.ACTION_DOWN;
        if (keyCode ==  KeyEvent.KEYCODE_HEADSETHOOK
                || keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                || keyCode == KeyEvent.KEYCODE_SPACE) {
            if (uniqueDown) {
                doPauseResume();
                show(sDefaultTimeout);
                if (mPauseButton != null) {
                    mPauseButton.requestFocus();
                }
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY) {
            if (uniqueDown && !mPlayer.isPlaying()) {
                mPlayer.start();
                updatePausePlay();
                show(sDefaultTimeout);
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_MEDIA_STOP
                || keyCode == KeyEvent.KEYCODE_MEDIA_PAUSE) {
            if (uniqueDown && mPlayer.isPlaying()) {
                mPlayer.pause();
                updatePausePlay();
                show(sDefaultTimeout);
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN
                || keyCode == KeyEvent.KEYCODE_VOLUME_UP
                || keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
            // don't show the controls for volume adjustment
            return super.dispatchKeyEvent(event);
        } else if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_MENU) {
            if (uniqueDown) {
                hide();
            }

            // Ensures that back still behaves normally.
            // Disable this to have back just hide on its own.
            Activity a = (Activity) mContext;
            a.finish();

            return true;
        }

        show(sDefaultTimeout);
        return super.dispatchKeyEvent(event);
    }

    private OnClickListener mPauseListener = new OnClickListener() {
        public void onClick(View v) {
            doPauseResume();
            show(sDefaultTimeout);
        }
    };

    private OnClickListener mMinControllerListener = new OnClickListener() {
        public void onClick(View v) {
            Intent myIntent = new Intent(mContext, DetailedMusicPlayerActivity.class);
            mContext.startActivity(myIntent);
        }
    };

    public void updatePausePlay() {

        if (mRoot == null || mPauseButton == null || mPlayer == null) {
            return;
        }

        if (mPlayer.isPlaying()) {
            mPauseButton.setImageResource(R.drawable.ic_media_pause);
        } else {
            mPauseButton.setImageResource(R.drawable.ic_media_play);
        }
    }

    private void doPauseResume() {
        if (mPlayer == null) {
            return;
        }

        if (mPlayer.isPlaying()) {
            mPlayer.pause();
        } else {
            mPlayer.start();
        }
        updatePausePlay();
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (mPauseButton != null) {
            mPauseButton.setEnabled(enabled);
        }
        disableUnsupportedButtons();
        super.setEnabled(enabled);
    }

    public interface MinMediaPlayerControl {
        void    start();
        void    pause();
        Song    getCurrentSong();
        boolean isPlaying();
        boolean canPause();
    }

    /**
     * Contains some magic to update the views seek bar.
     */
    private static class MessageHandler extends Handler {
        private final WeakReference<MinimalMusicControllerView> mView;

        MessageHandler(MinimalMusicControllerView view) {
            // WeakReference music allow you to access variables non statically.
            mView = new WeakReference<MinimalMusicControllerView>(view);
        }
        @Override
        public void handleMessage(Message msg) {
            MinimalMusicControllerView view = mView.get();
            if (view == null || view.mPlayer == null) {
                return;
            }

            int pos;
            switch (msg.what) {
                case FADE_OUT:
                    view.hide();
                    break;
                case SHOW_PROGRESS:
//                    pos = view.setProgress();
//                    if (!view.mDragging && view.mShowing && view.mPlayer.isPlaying()) {
//                        msg = obtainMessage(SHOW_PROGRESS);
//                        sendMessageDelayed(msg, 1000 - (pos % 1000));
//                    }
                    break;
            }
        }
    }
}