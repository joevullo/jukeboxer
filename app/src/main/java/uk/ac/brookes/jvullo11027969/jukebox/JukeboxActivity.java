package uk.ac.brookes.jvullo11027969.jukebox;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects.ApiSong;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects_adapters.ApiSongAdapter;
import uk.ac.brookes.jvullo11027969.main_activity.AbstractMusicPlayerBase;
import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.main_activity.settings.UserPreferenceActivity;

public class JukeboxActivity extends AbstractMusicPlayerBase {

    private ListView mListView;

    @Override
    public ViewGroup getAnchorPointView() {
        return (ViewGroup) findViewById(R.id.content_frame);
    }

    ArrayList dataItems = new ArrayList<ApiSong>();
    protected ApiSongAdapter       mSongAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jukeboxer);

        createTestSongs();
        mListView = (ListView) findViewById(android.R.id.list);
        mSongAdapter = new ApiSongAdapter(this, dataItems);
        mListView.setAdapter(mSongAdapter);

        registerForContextMenu(mListView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == android.R.id.list) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            //YourObject obj = (YourObject) lv.getItemAtPosition(acmi.position);
            menu.add("Vote up");
            menu.add("Vote down");
            //menu.add(obj.name);
        }
    }

    public void createTestSongs() {
        //for (int i = 0; i < 10; i++) {
            dataItems.add(new ApiSong(1, "Velo", "Kasabian", "Velo", 1));
            dataItems.add(new ApiSong(1, "Dark Side of The Moon",  "Pink Floyd", "Best Of", 1));
            dataItems.add(new ApiSong(1, "Shout", "Katy Perry", "Album", 1));
            dataItems.add(new ApiSong(1, "Test", "Meh", "Album", 1));
            dataItems.add(new ApiSong(1, "Before Dark", "The Kinks", "Album", 1));
            dataItems.add(new ApiSong(1, "After Dark", "The Kooks", "Album", 1));
        //}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        setTitle("Jukebox");
        getMenuInflater().inflate(R.menu.menu_jukeboxer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_settings:
                Intent intentForSettings = new Intent(this, UserPreferenceActivity.class);
                startActivity(intentForSettings);
                return true;
            case R.id.action_add_people:
                //Intent  = new Intent(this, UserPreferenceActivity.class);
                //startActivity(intentForSettings);
                return true;
            case R.id.action_add_songs:
                //Intent intentForSettings = new Intent(this, UserPreferenceActivity.class);
                //startActivity(intentForSettings);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}


















