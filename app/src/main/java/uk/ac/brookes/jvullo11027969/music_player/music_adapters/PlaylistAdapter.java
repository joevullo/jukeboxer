package uk.ac.brookes.jvullo11027969.music_player.music_adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Playlist;
import uk.ac.brookes.jvullo11027969.utils.SpecialSelectableAdapter;

/**
 * Created by jvullo on 10/01/15.
 */
public class PlaylistAdapter extends SpecialSelectableAdapter {

    public PlaylistAdapter(Context c, ArrayList musicItems) {
        super(c , musicItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout view = (LinearLayout) viewInflater.inflate
                (R.layout.playlist_item_view, parent, false);

        TextView playlistTitleView = (TextView) view.findViewById(R.id.playlist_name);
        Playlist currrent = (Playlist) dataList.get(position);
        playlistTitleView.setText(currrent.getTitle());

        setSelectedBackgroundView(view, position);

        return view;
    }
}