package uk.ac.brookes.jvullo11027969.main_activity;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;

import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

import uk.ac.brookes.jvullo11027969.music_player.music_objects.Song;
import uk.ac.brookes.jvullo11027969.utils.MyDebugger;

import static uk.ac.brookes.jvullo11027969.utils.MyDebugger.TAG_MUSIC_SERVICE;

/**
 * Used by the music mPlayer to play music from the device.
 *
 * Using a service allows audio to continue playing music into
 * the background.
 *
 * TODO, class needs obfuscating.
 * TODO, should this class exist here? Currently in the main activity.
 * TODO, notification should only appear when the background is in the foreground.
 * How can I not draw the notification if the music is not playing?.ø
 * TODO, notification should be cancellable.
 * TODO, audio and ducking implementation.
 */
public class MusicService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener, AudioManager.OnAudioFocusChangeListener  {

    private static final int MUSIC_NOTIFICATION_ID = 1;
    private static final String NOTIFICATION_DELETED_ACTION = "NOTIFICATION_DELETED";

    // These are the Intent actions that we are prepared to handle. Notice that the fact these
    // constants exist in our class is a mere convenience: what really defines the actions our
    // service can handle are the <action> tags in the <intent-filters> tag for our service in
    // AndroidManifest.xml.
    public static final String ACTION_TOGGLE_PLAYBACK =
            "com.example.android.musicplayer.action.TOGGLE_PLAYBACK";
    public static final String ACTION_PLAY = "com.example.android.musicplayer.action.PLAY";
    public static final String ACTION_PAUSE = "com.example.android.musicplayer.action.PAUSE";
    public static final String ACTION_STOP = "com.example.android.musicplayer.action.STOP";
    public static final String ACTION_SKIP = "com.example.android.musicplayer.action.SKIP";
    public static final String ACTION_REWIND = "com.example.android.musicplayer.action.REWIND";
    public static final String ACTION_URL = "com.example.android.musicplayer.action.URL";


    NotificationDeleteReceiver  mReceiver;
    protected Context           mContext;
    private Stack<Integer>      previouslyPlayedSongs;
    private ArrayList<Song>     currentPlaylist;
    private MediaPlayer         mPlayer;
    private int                 currentSongIndex = 0; // Represents the current playing song in the list.
    private IBinder             musicBind;

    //Vars for shuffle and repeat.
    private boolean             shuffle = false; // TODO, store the values from previous session.
    private boolean             repeat = false; // TODO, see above.

    // do we have audio focus?
    enum AudioFocus {
        NoFocusNoDuck,    // we don't have audio focus, and can't duck
        NoFocusCanDuck,   // we don't have focus, but can play at a low volume ("ducking")
        Focused           // we have full audio focus
    }
    AudioFocus mAudioFocus = AudioFocus.NoFocusNoDuck;


    public void onCreate(){
        super.onCreate();

        mContext = this;

        previouslyPlayedSongs = new Stack<Integer>();

        mPlayer = new MediaPlayer();
        musicBind = new MusicBinder();

        // Player setup stuff.
        setupMusicPlayer();

        this.mReceiver = new NotificationDeleteReceiver();
        registerReceiver(
                this.mReceiver,
                new IntentFilter(NOTIFICATION_DELETED_ACTION));

        // TODO
        // create the Audio Focus Helper, if the Audio Focus feature is available (SDK 8 or above)
//        if (android.os.Build.VERSION.SDK_INT >= 8)
//            mAudioFocusHelper = new AudioFocusHelper(getApplicationContext(), this);
//        else
//            mAudioFocus = AudioFocus.Focused; // no focus feature, so we always "have" audio focus


    }

    public void setupMusicPlayer() {
        // Player setup stuff.
        mPlayer.setWakeMode(getApplicationContext(),PowerManager.PARTIAL_WAKE_LOCK);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnErrorListener(this);
    }

    public MusicService() {}

    @Override
    public void onAudioFocusChange(int focusChange) {

    }

    //TODO, replace with an alternative? which works across the application.
    public class MusicBinder extends Binder {
        MusicService getService() {
            return MusicService.this;
        }
    }

    /**
     * Retrieves the current playlist.
     * @return
     */
    public ArrayList<Song> getCurrentPlaylist() {
        return currentPlaylist;
    }

    /**
     * Used to get the details of the song current playing.
     * This allows the minimal view
     *
     * @return
     */
    public Song getCurrentSong() {
        if (getCurrentPlaylist() != null) {
            return getCurrentPlaylist().get(currentSongIndex);
        }

        return null;
    }

    /**
     * Add an arraylist of songs to the current playlist.
     * @param songsToAdd
     */
    public void addToCurrentPlaylist(ArrayList<Song> songsToAdd) {
        currentPlaylist.addAll(songsToAdd);
    }

    /**
     * Sets the playlist.
     * @param newPlaylist
     */
    public void setPlaylist(ArrayList<Song> newPlaylist) {
        previouslyPlayedSongs.clear();
        currentPlaylist = newPlaylist;
    }

    @Override
    public IBinder onBind(Intent intent) {
        MyDebugger.log(MyDebugger.TAG_MUSIC_SERVICE, "Music service is binding." , null, "v");
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent){
        MyDebugger.log(MyDebugger.TAG_MUSIC_SERVICE, "Music service is unbinding." , null, "v");
        mPlayer.stop();
        mPlayer.release();
        return true;
    }

    /**
     * @return
     */
    public boolean isPlaylistEmpty() { return currentPlaylist.isEmpty(); }

    /**
     * Checks whether the position is a valid, logs any issues.
     */
    public boolean isPositionIsValid(int i, long id) {

        if (i > 0 && i < currentPlaylist.size()) {
            if (currentPlaylist.get(i).getId() == id) {
                return true;
            } else {
                MyDebugger.log(TAG_MUSIC_SERVICE, "Error: Playlist has likely " +
                        "become misaligned.", null, "e");
            }
        } else {
            MyDebugger.log(TAG_MUSIC_SERVICE, "i was not in the range of " +
                    "the currentPlaylist.", null, "e");
        }

        MyDebugger.log(TAG_MUSIC_SERVICE, "There was an error in selecting the " +
                "song. Position 0 was set instead.", null, "e");

        return false;
    }

    /**
     * Sets the current playing song.
     * @param i
     */
    private void setNewSong(int i) {
        currentSongIndex = i;
    }

    /**
     * On the completion of a song the mPlayer will 'reset' and then play
     * next song.
     * @param mp
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        if(mPlayer.getCurrentPosition() > 0){
            mp.reset();
            playNextSong();
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        MyDebugger.log(TAG_MUSIC_SERVICE, "Playback Error", null, "e");
        mp.reset();
        return false;
    }

    @Override
    public void onDestroy() {
        mPlayer.release();
        stopForeground(true); //Should stop the notification in the background.
        unregisterReceiver(mReceiver);
    }

    /** Called when media mPlayer is done preparing. */
    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
        drawMusicNotification();
    }

    private void cancelMusicNotification() {
        stopForeground(true); //Should stop the notification in the background.
    }

    /**
     * Draws a notification.
     */
    private void drawMusicNotification() {
        Intent notIntent = new Intent(this, JukeboxerMainActivity.class);
        notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendInt = PendingIntent.getActivity(this, 0,
                notIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intent = new Intent(NOTIFICATION_DELETED_ACTION);
        PendingIntent pendDelIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);

        Notification.Builder builder = new Notification.Builder(this);

        Notification notification = new Notification.Builder(this)
                .setContentIntent(pendInt)
                .setContentTitle(currentPlaylist.get(currentSongIndex).getTitle())
                .setContentText(currentPlaylist.get(currentSongIndex).getArtist())
                .setTicker(currentPlaylist.get(currentSongIndex).getAlbum())
                 //.setOngoing(true) // This is stopping the notification from being removed. s
                .setAutoCancel(true)
                .setDeleteIntent(pendDelIntent)
                .setSmallIcon(R.drawable.ic_launcher).getNotification();

        builder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;

//        notification.notify();

        startForeground(MUSIC_NOTIFICATION_ID, notification); // Shows the notification in the foreground. n
    }

    /**
     * Used to set the playlist and the position of the song instantly.
     * The Id is used for checking the position selected matches the selected song.
     *
     * @param newPlaylist
     * @param startPosition
     */
    public void startMusicPlayer(ArrayList<Song> newPlaylist, int startPosition) {

        // Set the new playlist.
        setPlaylist(newPlaylist);
        // Sets the songs position & play.
        setNewSong(startPosition);
        playSong();
    }

    /**
     * Sets a new song position straight away and then
     * starts the mPlayer.
     *
     * @param newPosition
     */
    public void selectAndPlaySong(int newPosition) {
        setNewSong(newPosition);
        playSong();
    }

    /**
     * Used to play a song, only the inner class should have access to this method.
     */
    private void playSong() {
        mPlayer.reset();
        Song currentSong = currentPlaylist.get(currentSongIndex);
        Uri trackUri = ContentUris.withAppendedId(
                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                currentSong.getId());
        try{
            mPlayer.setDataSource(getApplicationContext(), trackUri);

            // Choosing to prepare rather than asyncPrepare means that
            // method to update pause & play works correctly. Previously
            // the method updatePausePlay in the media controller would run
            // before the music started and therefore set the play button instead of the pause.

            // This may become an issue later since async is used for audio
            // streams.

            // mPlayer.prepareAsync(); Enable the streams.
            mPlayer.prepare(); // Replaces prepare Async.
            mPlayer.start();

        }
        catch(Exception e){
            MyDebugger.log(TAG_MUSIC_SERVICE, "Error setting data source", null, "e");
        }
    }

    /**
     * Play the previous song in the current playlist.
     */
    public void playPreviousSong() {
        if (shuffle) {
            currentSongIndex = previouslyPlayedSongs.pop();
            playSong();
        } else {
            currentSongIndex--;
            if (currentSongIndex < 0) {
                currentSongIndex = currentPlaylist.size() - 1;
            }
            playSong();
        }
    }

    /**
     * Play the next song in the current playlist.
     */
    public void playNextSong() {
        if(shuffle) {
            Random rand = new Random();
            int newSong = currentSongIndex;
            while(newSong == currentSongIndex){
                newSong = rand.nextInt(currentPlaylist.size());
            }
            previouslyPlayedSongs.push(currentSongIndex);
            currentSongIndex = newSong;

            playSong();

        } else{
            currentSongIndex++;
            if(currentSongIndex >= currentPlaylist.size()) {
                currentSongIndex = 0;

                // If you've reached the ending, then only playSong if the repeat is available.
                if (repeat) {
                    playSong();
                }
            }
            playSong();
        }

    }

    public boolean isShuffleEnabled() { return shuffle; }

    public boolean isRepeatEnabled() { return repeat; }

    public void setShuffle(boolean newValue) {
        previouslyPlayedSongs.clear(); //Clear the playlist since shuffle has changed.
        shuffle = newValue;
    }

    public void setRepeat(boolean newValue) { repeat = newValue; }

    public int getPosn(){ return mPlayer.getCurrentPosition(); }

    public int getDur(){ return mPlayer.getDuration(); }

    public boolean isPng(){ return mPlayer.isPlaying(); }

    public void pausePlayer(){
        mPlayer.pause();
        cancelMusicNotification();
        // TODO, perhaps remove the notification, when paused get the notification and cancel it.
    }

    public void seek(int posn){ mPlayer.seekTo(posn); }

    public void go(){ mPlayer.start(); }


    /**
     * Declared in inner class of the Music Service, this reciever is added in the android
     * manifest and will respond to the user cancelling the intent which will
     * result in the music stopping.
     */
    public class NotificationDeleteReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Be careful, depending upon flag used to bind this may
            // Not close the service unless the all the connections are unbound.

            // if (!isPng())
                ((Service) mContext).stopSelf();
            MyDebugger.log(TAG_MUSIC_SERVICE, "Notification Delete Received", null, "v");
        }
    }

    /**
     * Releases resources used by the service for playback. This includes the "foreground service"
     * status and notification, the wake locks and possibly the MediaPlayer.
     *
     * @param releaseMediaPlayer Indicates whether the Media Player should also be released or not
     */
    void relaxResources(boolean releaseMediaPlayer) {
        // stop being a foreground service
        stopForeground(true);

        // stop and release the Media Player, if it's available
        if (releaseMediaPlayer && mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
        }
    }

    // TODO
//    void tryToGetAudioFocus() {
//        if (mAudioFocus != AudioFocus.Focused && mAudioFocusHelper != null
//                && mAudioFocusHelper.requestFocus())
//            mAudioFocus = AudioFocus.Focused;
//    }
}
