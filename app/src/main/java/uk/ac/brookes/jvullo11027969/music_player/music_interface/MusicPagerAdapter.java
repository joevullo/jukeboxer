package uk.ac.brookes.jvullo11027969.music_player.music_interface;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import uk.ac.brookes.jvullo11027969.main_activity.DefaultNavigationObjectFragment;
import uk.ac.brookes.jvullo11027969.music_player.music_fragments.AlbumsFragment;
import uk.ac.brookes.jvullo11027969.music_player.music_fragments.ArtistsFragment;
import uk.ac.brookes.jvullo11027969.music_player.music_fragments.PlaylistsFragment;
import uk.ac.brookes.jvullo11027969.music_player.music_fragments.SongsFragment;

import static uk.ac.brookes.jvullo11027969.utils.Constants.MUSIC_NAVIGATION_TABS;

/**
 * Created by jvullo on 02/12/14.
 */

public class MusicPagerAdapter  extends FragmentPagerAdapter {

    public MusicPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public android.support.v4.app.Fragment getItem(int i) {

        switch (i) {
            case 0:
                return new SongsFragment();
            case 1:
                return new ArtistsFragment();
            case 2:
                return new AlbumsFragment();
            case 3:
                return new PlaylistsFragment();
            default:
                return new DefaultNavigationObjectFragment(); //Returns a default fragment in case the switch doesn't work out.
        }
    }

    @Override
    public int getCount() {
        return MUSIC_NAVIGATION_TABS.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "" +  MUSIC_NAVIGATION_TABS[position];
    }

}
