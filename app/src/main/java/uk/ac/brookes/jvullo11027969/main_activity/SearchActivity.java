package uk.ac.brookes.jvullo11027969.main_activity;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.music_player.music_adapters.AlbumAdapter;
import uk.ac.brookes.jvullo11027969.music_player.music_adapters.ArtistAdapter;
import uk.ac.brookes.jvullo11027969.music_player.music_adapters.PlaylistAdapter;
import uk.ac.brookes.jvullo11027969.music_player.music_adapters.SongAdapter;
import uk.ac.brookes.jvullo11027969.music_player.music_database.MusicDatabaseConnector;
import uk.ac.brookes.jvullo11027969.music_player.music_interface.SelectiveMusicList;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.AbstractMusic;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Album;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Artist;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Playlist;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Song;
import uk.ac.brookes.jvullo11027969.utils.Constants;

/**
 * TODO, add the playing of music functionality.
 * TODO, deal with the listview in scrollview problems.
 * TODO, offer the gridview for larger devices.
 */
public class SearchActivity extends AbstractMusicPlayerBase implements ListView.OnItemClickListener , SearchView.OnQueryTextListener {

    private TextView            emptyItemView; // Used to set text to show that the view is empty.

    // Music Items list.
    private ListView            mSongListView;

    private ListView            mArtistListView;
    private ListView            mAlbumListView;
    private ListView            mPlaylistListView;

    // Music Items list.
    private ArrayList           mSongList;
    private ArrayList           mArtistList;
    private ArrayList           mAlbumList;
    private ArrayList           mPlaylistList;

    // Adapters for lists.
    protected SongAdapter       mSongAdapter;
    protected ArtistAdapter     mArtistAdapter;
    protected AlbumAdapter      mAlbumAdapter;
    protected PlaylistAdapter   mPlaylistAdapter;

    // Connection to get a out.
    protected MusicDatabaseConnector mMDC;

    @Override
    public ViewGroup getAnchorPointView() {
        return (ViewGroup) findViewById(R.id.content_frame);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        emptyItemView = (TextView) findViewById(android.R.id.empty);
        emptyItemView.setVisibility(View.GONE); //Set this to gone initially.

        setTitle("Search");

        // Setup the array lists.
        mSongList = new ArrayList<Song>();
        mArtistList = new ArrayList<Artist>();
        mAlbumList = new ArrayList<Album>();
        mPlaylistList = new ArrayList<Playlist>();

        mMDC = new MusicDatabaseConnector(getContentResolver());

        mSongListView = (ListView) findViewById(R.id.listViewSongs);
        mArtistListView = (ListView) findViewById(R.id.listViewArtists);
        mAlbumListView = (ListView) findViewById(R.id.listViewAlbums);
        mPlaylistListView = (ListView) findViewById(R.id.listViewPlaylists);

        mSongListView.setOnItemClickListener(this);
        mArtistListView.setOnItemClickListener(this);
        mAlbumListView.setOnItemClickListener(this);
        mPlaylistListView.setOnItemClickListener(this);

        // Hide the heading initially.
        findViewById(R.id.songs_listview_heading).setVisibility(View.GONE);
        findViewById(R.id.artists_listview_heading).setVisibility(View.GONE);
        findViewById(R.id.album_listview_heading).setVisibility(View.GONE);
        findViewById(R.id.playlist_listview_heading).setVisibility(View.GONE);

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchForMusicAndStoreResults(query);
            // Now draw the view.
            drawView();
        }
    }

    public void drawView() {

        // If any of the listview items are empty we don't want to show the headings, so make the views invisible.
        if (!mSongList.isEmpty()) {
            findViewById(R.id.songs_listview_heading).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.songs_listview_heading).setVisibility(View.GONE);
        }

        if (!mArtistList.isEmpty()) {
            findViewById(R.id.artists_listview_heading).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.artists_listview_heading).setVisibility(View.GONE);
        }

        if  (!mAlbumList.isEmpty()) {
            findViewById(R.id.album_listview_heading).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.album_listview_heading).setVisibility(View.GONE);
        }

        if (!mPlaylistList.isEmpty()) {
            findViewById(R.id.playlist_listview_heading).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.playlist_listview_heading).setVisibility(View.GONE);
        }

        // If they are all empty then
        if (!mSongList.isEmpty() && mArtistList.isEmpty() && mAlbumList.isEmpty() && mPlaylistList.isEmpty()) {
            showAndSetEmptyTextView("The search found no similar music.");
        } else {
            // The music adapter will be set by the subclasses.
            initialiseAdapters();
            // Sets the adapter to the views.
            mAlbumListView.setAdapter(mAlbumAdapter);
            mSongListView.setAdapter(mSongAdapter);
            mArtistListView.setAdapter(mArtistAdapter);
            mPlaylistListView.setAdapter(mPlaylistAdapter);
        }
    }

    /**
     * Sets up the adapters.
     */
    public void initialiseAdapters() {
        mSongAdapter = new SongAdapter(this, mSongList);
        mArtistAdapter = new ArtistAdapter(this , mArtistList);
        mAlbumAdapter = new AlbumAdapter(this, mAlbumList);
        mPlaylistAdapter = new PlaylistAdapter(this, mPlaylistList);
    }


    /**
     * This will show the empty text view and the set the text within it
     * to the CharSequence parameter passed with it.
     *
     * This view is used to display to the user that there are no search results.
     */
    public void showAndSetEmptyTextView(CharSequence emptyText) {
        if (emptyItemView != null) {
            // emptyItemView.setVisibility(View.VISIBLE);
            emptyItemView.setText(emptyText);
        }
    }


    /**
     * Performs the
     *
     * @param searchQuery
     */
    public void searchForMusicAndStoreResults(String searchQuery) {
        // Get data. But if the string is empty we should
        // Keep the arraylist empty.
        if (!searchQuery.isEmpty()) {
            mSongList = mMDC.detailedSearchForSong(searchQuery);
            mArtistList = mMDC.searchForArtistsTitle(searchQuery);
            mAlbumList = mMDC.searchForAlbumsTitle(searchQuery);
            mPlaylistList = mMDC.searchForPlaylistTitle(searchQuery);
        } else {
            mSongList.clear();
            mArtistList.clear();
            mAlbumList.clear();
            mPlaylistList.clear();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName(getApplicationContext(), SearchActivity.class)));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        ArrayList <AbstractMusic> musicList = null;
        String musicMode = Constants.UNKNOWN_MODE;

        // Sorts out the which of the listviews were clicked.
        if (parent.equals(mAlbumListView)) {
            musicList = mAlbumList;
            musicMode = Constants.ALBUM_MODE;
        } else if (parent.equals(mSongListView)) {
            musicList = mSongList;
            musicMode = Constants.SONG_MODE;
        } else if (parent.equals(mArtistListView)) {
            musicList = mArtistList;
            musicMode = Constants.ARTIST_MODE;
        } else if (parent.equals(mPlaylistListView)) {
            musicList = mPlaylistList;
            musicMode = Constants.PLAYLIST_MODE;
        }

        AbstractMusic currentMusic = musicList.get(position);

        if (musicMode.equals(Constants.SONG_MODE)) {

            ArrayList<Song> newSongList = new ArrayList<Song>();

            // Converting this to a song list.
            for (int i = 0; i < musicList.size(); i++) {
                newSongList.add((Song) musicList.get(i));
            }

            startMusicPlayer(newSongList, 0);
            return;
        }

        Intent myIntent = new Intent(this, SelectiveMusicList.class);
        myIntent.putExtra(Constants.KEY_MUSIC_TYPE_MODE, musicMode);
        myIntent.putExtra(Constants.KEY_MUSIC_ITEM_ID, currentMusic.getId()); //Key, the value
        myIntent.putExtra(Constants.KEY_MUSIC_NAME, currentMusic.getTitle());

        this.startActivity(myIntent);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        searchForMusicAndStoreResults(newText);
        if (newText.isEmpty()) {
            setTitle("Search");
        } else {
            setTitle(newText);
        }
        drawView();

        return false;
    }
}
