package uk.ac.brookes.jvullo11027969.main_activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import uk.ac.brookes.jvullo11027969.jukebox.JukeboxFragment;
import uk.ac.brookes.jvullo11027969.music_player.music_interface.MusicTabManagerFragment;
import uk.ac.brookes.jvullo11027969.share.ShareFragment;
import uk.ac.brookes.jvullo11027969.social.SocialTabManagerFragment;

import static uk.ac.brookes.jvullo11027969.utils.Constants.MAIN_NAVIGATION_ITEMS;

/**
 * Intially used for trying out tabs as the main navigation.
 *
 * Created by jvullo on 30/11/14.
 */
// Since this is an object collection, use a FragmentStatePagerAdapter,
// and NOT a FragmentPagerAdapter.
public class JukeboxerPagerAdapter extends FragmentStatePagerAdapter {
    public JukeboxerPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public android.support.v4.app.Fragment getItem(int i) {

        switch (i) {
            case 0:
                return new MusicTabManagerFragment();
            case 1:
                return new ShareFragment();
            case 2:
                return new JukeboxFragment();
            case 3:
                return new SocialTabManagerFragment();
            default:
                Fragment fragment = new DefaultNavigationObjectFragment();
                Bundle args = new Bundle();
                // Our object is just an integer :-P
                args.putInt(DefaultNavigationObjectFragment.ARG_OBJECT, i + 1);
                fragment.setArguments(args);
                return fragment;
        }
    }

    @Override
    public int getCount() {
        return MAIN_NAVIGATION_ITEMS.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "OBJECT " + (position + 1);
    }
}
