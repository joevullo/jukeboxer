package uk.ac.brookes.jvullo11027969.music_player.music_fragments;

import android.widget.AbsListView;
import android.widget.ListView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.music_player.music_adapters.AlbumAdapter;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AsyncMusicDBQuery;
import uk.ac.brookes.jvullo11027969.utils.Constants;

/**
 * A fragment that implements a view that represents a song by extending the
 * AbstractMusicBaseFragment and overriding the
 *
 *
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 *
 */
public class AlbumsFragment extends AbstractMusicBaseFragment implements AbsListView.OnItemClickListener {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AlbumsFragment() {
        musicModeType = Constants.ALBUM_MODE;
    }

    @Override
    public void initialiseAdapter(ArrayList musicList) {
        musicAdapter = new AlbumAdapter(getActivity(), musicList);
    }

    @Override
    public void onResume() {
        super.onResume();
        // We are temp disabling the multichoice since its options
        // are not implemented for the artist view.
        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    @Override
    public AsyncMusicDBQuery buildAsyncMusicDataQuery() {
        AsyncMusicDBQuery a = new AsyncMusicDBQuery(AsyncMusicDBQuery.GET_ALL_ALBUM_MODE);
        return a;
    }

}
