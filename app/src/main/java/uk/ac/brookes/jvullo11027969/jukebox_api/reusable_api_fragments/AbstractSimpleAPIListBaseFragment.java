package uk.ac.brookes.jvullo11027969.jukebox_api.reusable_api_fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.view.ActionMode;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

import uk.ac.brookes.jvullo11027969.jukebox_api.api_connector.AsyncJukeboxAPIConnector;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_connector.JukeboxAPIQuery;
import uk.ac.brookes.jvullo11027969.jukebox_api.api_objects_adapters.ApiUserAdapter;
import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.utils.MyDebugger;
import uk.ac.brookes.jvullo11027969.utils.SpecialSelectableAdapter;

/**
 * A simple class which will use a fragment to present a simple list view populated with data called
 * from the API.
 *
 * This class also provides the functionality to do CAB multiselection, but is not enabled by default
 * unless activeCABSelection is called.
 *
 * This fragment will be overridden to represent different API data, see FriendsFragmentBase and NearbyUsersFragments.
 *
 */
abstract public class AbstractSimpleAPIListBaseFragment extends Fragment implements ListView.OnItemClickListener ,
       AsyncJukeboxAPIConnector.APIDataReceivedListener, AbsListView.MultiChoiceModeListener {

    protected OnFragmentInteractionListener     mListener;
    private View                                rootView;
    protected ArrayList                         apiDataList;
    protected String[]                          simpleListNames;
    protected SpecialSelectableAdapter          dataAdapter;
    protected AbsListView                       mListView;
    protected ActionMode                        mActionMode;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AbstractSimpleAPIListBaseFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        //getActivity().requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
    }

    /**
     * Used to setup the initial view, including calling for the initial data
     * that is shown in the view.
     *
     * TODO, a loading item view after calling the data async.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_music, container, false);

        mListView = (AbsListView) rootView.findViewById(android.R.id.list);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);
        mListView.setEmptyView(rootView.findViewById(android.R.id.empty));
        mListView.setFastScrollEnabled(true);

        // Passing in this fragment as the listener, we are also calling
        // buildAsyncAPIDataQuery() which will build a query that the
        // api connector will recognise.

        getAPIDataAsync(this, buildAsyncAPIDataQuery());

        return rootView;
    }

    /**
     * When called will allow the user to multiselect the required items.
     */
    protected void enableMultiselect() {
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(this);
    }

//    @Override
//    public void onViewCreated(View view, Bundle savedInstanceState) {
//        getActivity().requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
//        getActivity().setProgressBarIndeterminateVisibility(true);
//    }

    protected void setParentActivityTitle(String title) {
        if (getActivity() instanceof FragmentActivity) {
            ((FragmentActivity) getActivity()).setTitle(title);
        } else if (getActivity() instanceof ActionBarActivity) {
            ((ActionBarActivity) getActivity()).setTitle(title);
        } else {
            MyDebugger.log(MyDebugger.TAG_FRIENDS_FRAGMENT, "Could not set title.", null, "e");
        }
    }

    @Override
    public void onAPIDataReceivedPrepared(ArrayList result) {

        // If getActivity is null, then we've loss the context and we shouldn't
        // Try drawing the view, this is not a great solution, ideally we should
        // be cancelling the async tasks using a lsitener if we no longer
        // care about it.

        // I don't get why this works, because the activity might still exist but the
        // fragment won't, which could cause the issue?
        // But android only cares about "context" of activities.

        if (getActivity() != null) {
            apiDataList = result;
            drawView();
        }
    }

    /**
     * Calls for the data on the device and draws that data on a list
     * view.
     */
    public void drawView() {
        if (apiDataList.isEmpty()) {
            setEmptyText("Item not found");
        } else {
            // The music adapter will be set by the subclasses.
            initialiseAdapter(apiDataList);
            // Set the adapter
            //((AdapterView<ListAdapter>) mListView).setAdapter(musicAdapter);
            ((AdapterView<ListAdapter>) mListView).setAdapter(dataAdapter);
        }

        // Either way we should be removing this because the query has returned.
        getActivity().setProgressBarIndeterminateVisibility(false);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    /**
     * Builds the query, this will depend on the fragment as it will decide what query
     * should be sent and retrieved to the API. For example, a query could be built
     * that asks for all users.
     */
    abstract public JukeboxAPIQuery buildAsyncAPIDataQuery();

    public void getAPIDataAsync(AsyncJukeboxAPIConnector.APIDataReceivedListener mListener,
                                JukeboxAPIQuery dBQuery) {

        getActivity().setProgressBarIndeterminateVisibility(true);
        AsyncJukeboxAPIConnector jukeboxAPI = new AsyncJukeboxAPIConnector(getActivity(), mListener);
        jukeboxAPI.execute(dBQuery);
    }

    /**
     *
     * Sets up the adapter with the array list passed as a parameter.
     * This needs to be set in the sub classes so that you can specify
     * the adapter for the type of api data recieved.
     *
     * @param apiDataList - Used to set the relevant adapter.
     */
    //abstract public void initialiseAdapter(ArrayList apiDataList);
    // For now, we are just using a simple list adapter.
    public void initialiseAdapter(ArrayList apiDataList) {
        dataAdapter = new ApiUserAdapter(getActivity(), apiDataList);
        //dataAdapter = new SpecialArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, simpleListNames);
    }

    /**
     * Used to launch the selective music view with the relevant information for the view.
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //MusicTabManager a = (MusicTabManager) getParentFragment();
        //a.switchFragment();

        //AbstractMusic currentMusic = (AbstractMusic) apiDataList.get(position);

        //Intent myIntent = new Intent(getActivity(), SelectiveMusicList.class);
        //myIntent.putExtra(Constants.KEY_MUSIC_TYPE_MODE, getCurrentMusicMode());
        //myIntent.putExtra(Constants.KEY_MUSIC_ITEM_ID, currentMusic.getId()); //Key, the value
        //myIntent.putExtra(Constants.KEY_MUSIC_NAME, currentMusic.getTitle());

        //getActivity().startActivity(myIntent);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

        // If we are detaching we should also remove the loading icon because we no longer
        // care about the data returning, or at least representing that to the user.
        getActivity().setProgressBarIndeterminateVisibility(false);
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView)
            ((TextView) emptyView).setText(emptyText);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }


    protected ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();

            // TODO, inflate a differenent menu for different options.
            inflater.inflate(R.menu.abstract_music_base_context_menu, menu);

            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_playlist_add:
                    //shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position,
                                          long id, boolean checked) {
        // Here you can do something when items are selected/de-selected,
        // such as update the title in the CAB

        if (checked) {
            //nr++;
            dataAdapter.setNewSelection(position, checked);
        } else {
            //nr--;
            dataAdapter.removeSelection(position);
        }
        //mode.setTitle(nr + " rows selected!");

    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

        mListView.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

        // Respond to clicks on the actions in the CAB
        StringBuilder sb = new StringBuilder();
        Set<Integer> positions = dataAdapter.getCurrentCheckedPositions();
        for (Integer pos : positions) {
            sb.append(" " + pos + ",");
        }
        switch (item.getItemId()) {
            case R.id.action_playlist_add:
                Toast.makeText(getActivity(), "Edited entries: " + sb.toString(),
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_playlist_remove:
                Toast.makeText(getActivity(), "Deleted entries : " + sb.toString(),
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_settings:
                dataAdapter.clearSelection();
                Toast.makeText(getActivity(), "Finish the CAB!",
                        Toast.LENGTH_SHORT).show();
                mode.finish();
        }
        return false;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        // Inflate the menu for the CAB
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.abstract_music_base_context_menu, menu);

        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        // Here you can make any necessary updates to the activity when
        // the CAB is removed. By default, selected items are deselected/unchecked.
        dataAdapter.clearSelection();

    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        // Here you can perform updates to the CAB due to
        // an invalidate() request
        return false;
    }
}