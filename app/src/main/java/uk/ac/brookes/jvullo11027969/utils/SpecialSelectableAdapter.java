package uk.ac.brookes.jvullo11027969.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.AbstractMusic;

/**
 * This class will allow CAB selection within an adapter.
 *
 * Just override the view and go wild.
 *
 * Created by jvullo on 10/01/15.
 */
abstract public class SpecialSelectableAdapter extends BaseAdapter {

    protected Context                   c;
    protected ArrayList                 dataList = new ArrayList(); // Giving this an initial value to avoid null errors.
    protected LayoutInflater            viewInflater;
    private HashMap<Integer, Boolean>   mSelection = new HashMap<Integer, Boolean>();

    public SpecialSelectableAdapter(Context c, ArrayList dataList) {
        // We might have lost the context if we've moved activites or
        // closed the application, which produces a null.
        if (c != null) {
            this.dataList = dataList;
            viewInflater = LayoutInflater.from(c);
            this.c = c;
        }
    }

    public HashMap<Integer, Boolean> getHashMapSelected() {
        return mSelection;
    }

    public void setHashMapSelected(HashMap<Integer, Boolean> mNewSelected) {
        mSelection = mNewSelected;
    }

    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
        notifyDataSetChanged();
    }

    public boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return result == null ? false : result;
    }

    public Set<Integer> getCurrentCheckedPositions() {
        return mSelection.keySet();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }

    /**
     * Uses the mSelection array to set the background of view passed in
     * if the item has been selected by the user.
     *
     * @param v - The view to set the color background.
     * @param position - The position is used to check the selected items array.
     */
    public void setSelectedBackgroundView(View v, int position) {

        //v.setBackgroundColor(Color.parseColor("#99cc00")); //default color

        if (mSelection.get(position) != null) {
            v.setBackgroundColor(c.getResources().getColor(R.color.selection_color));// this is a selected position so make it red
        }

        //v.setText("#" + Integer.toHexString(color).replaceFirst("ff", ""));
        //v.setBackgroundColor(color);
    }

    @Override
    public int getCount() { return dataList.size(); }

    @Override
    public Object getItem(int position) {return dataList.get(position); }

    @Override
    public long getItemId(int position) {

        // Erm, can't remember why I did this.
        if (position < 0 || position >= dataList.size()) {
            return -1;
        }

        // If we are dealing with Abstract Music types then we can get the ID.
        boolean isMusicType = dataList.get(position) instanceof AbstractMusic;
        if (isMusicType) {
            AbstractMusic m = (AbstractMusic) dataList.get(position);
            return m.getId();
        }

        // Otherwise just return the position.
        return position;
    }

    @Override
    abstract public View getView(int position, View convertView, ViewGroup parent);

    @Override
    public boolean hasStableIds() {
        return true;
    }
}