package uk.ac.brookes.jvullo11027969.music_player.music_fragments;

import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.main_activity.JukeboxerMainActivity;
import uk.ac.brookes.jvullo11027969.music_player.music_adapters.SongAdapter;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AsyncMusicDBQuery;
import uk.ac.brookes.jvullo11027969.utils.Constants;

/**
 * A fragment that implements a view that represents a song by extending the
 * AbstractMusicBaseFragment and overriding the
 *
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class SongsFragment extends AbstractMusicBaseFragment implements AbsListView.OnItemClickListener {
    
    /**
     * Mandatory empty constructor forthe fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SongsFragment() {
        musicModeType = Constants.SONG_MODE;
    }

    @Override
    public void initialiseAdapter(ArrayList musicList) {
        musicAdapter = new SongAdapter(getActivity(), musicList);
    }

    @Override
    public AsyncMusicDBQuery buildAsyncMusicDataQuery() {
        AsyncMusicDBQuery a = new AsyncMusicDBQuery(AsyncMusicDBQuery.GET_ALL_SONGS_MODE);
        return a;
    }

    /**
     * This has different behaviour, pressing a song should play music.
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Toast.makeText(getActivity(), "Music playback has not been implemented yet.", Toast.LENGTH_SHORT).show();

        JukeboxerMainActivity a = (JukeboxerMainActivity) getActivity();
        a.startMusicPlayer(musicList, position);
    }
}

