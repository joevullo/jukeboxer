package uk.ac.brookes.jvullo11027969.jukebox_api.api_connector;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;


/**
 * Created by jvullo on 01/02/15.
 *
 */
public class AsyncJukeboxAPIConnector extends AsyncTask<Object, Void, ArrayList> {

    JukeboxAPIQuery apiQuery;
    JukeboxAPIConnector mAPIConnector;
    APIDataReceivedListener mListener;

    public AsyncJukeboxAPIConnector(Context mContext, APIDataReceivedListener listener) {
        mAPIConnector = new JukeboxAPIConnector(mContext);
        mListener = listener;
    }

    @Override
    protected ArrayList doInBackground(Object... arg0) {

        // If we are using fake test data, then we should simulate
        // The data taking a little bit more time to retrieve
        // like it would occur if we were actually using a data
        // connection to get to it
        if (mAPIConnector.ENABLE_TEST_DATA) {
            try {
                Thread.sleep(2000);                 //1000 milliseconds is one second.
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }

        ArrayList returnValue = null;

        //Get query using the helper
        apiQuery = (JukeboxAPIQuery) arg0[0];

        switch (apiQuery.getCommandType()) {
            case JukeboxAPIQuery.GET_USERS_MODE:
                returnValue = mAPIConnector.getUsers();
                break;
            case JukeboxAPIQuery.GET_USERS_MESSAGES_MODE:
                returnValue = mAPIConnector.getUsersMessages(apiQuery.getId());
                break;
            case JukeboxAPIQuery.GET_USERS_FRIENDS_MODE:
                returnValue = mAPIConnector.getUsersFriends();
                break;
            case JukeboxAPIQuery.GET_JUKEBOXES_NEARBY:
                returnValue = mAPIConnector.getNearbyJukebox();
                break;
            case JukeboxAPIQuery.GET_STREAMS_NEARBY:
                returnValue = mAPIConnector.getNearbyStreams();
                break;
            default:
                returnValue = null;
                break;
        }

        return returnValue;
    }

    @Override
    protected void onPostExecute(ArrayList result) {

        // This is a bit cheeky but we are ensuring that
        // at the very least that we return an empty list.
        if (result == null) {
            result = new ArrayList();
        }

        mListener.onAPIDataReceivedPrepared(result);
    }

    public interface APIDataReceivedListener {
        public void onAPIDataReceivedPrepared(ArrayList result);
    }
}