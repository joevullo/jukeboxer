package uk.ac.brookes.jvullo11027969.music_player.music_adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_database.AlbumArtAsyncImageLoader;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Album;
import uk.ac.brookes.jvullo11027969.utils.SpecialSelectableAdapter;

/**
 * Created by jvullo on 10/01/15.
 */
public class AlbumAdapter extends SpecialSelectableAdapter {

    public AlbumAdapter(Context c, ArrayList musicItems) {
        super(c , musicItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout view = (LinearLayout) viewInflater.inflate
                (R.layout.album_item_view, parent, false);

        TextView albumTitleView = (TextView) view.findViewById(R.id.album_title);
        TextView albumArtistView = (TextView) view.findViewById(R.id.album_artist);
        ImageView albumArtView = (ImageView) view.findViewById(R.id.album_art);

        Album currrentAlbum = (Album) dataList.get(position);

        albumTitleView.setText(currrentAlbum.getTitle());
        albumArtistView.setText(currrentAlbum.getArtist());

        //if (currrentAlbum.hasAlbumArt())
        //    albumArtView.setImageBitmap(currrentAlbum.getAlbumArtBitmap());

        // Loads the album art asynchronous
        new AlbumArtAsyncImageLoader().execute(albumArtView, currrentAlbum.getId() , c);

        setSelectedBackgroundView(view, position);

        return view;
    }
}