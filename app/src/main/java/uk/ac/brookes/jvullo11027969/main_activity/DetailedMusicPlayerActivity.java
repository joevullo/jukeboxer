package uk.ac.brookes.jvullo11027969.main_activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import uk.ac.brookes.jvullo11027969.main_activity.settings.UserPreferenceActivity;
import uk.ac.brookes.jvullo11027969.music_player.music_interface.NowPlayingPlaylist;

/**
 * A class that implements a detailed view for controlling music.
 */
public class DetailedMusicPlayerActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_view);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detailed_music_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.action_show_playlist:
                Intent nowPlaying = new Intent(this, NowPlayingPlaylist.class);
                startActivity(nowPlaying);
                return true;
            case R.id.action_settings:
                Intent intentForSettings = new Intent(this, UserPreferenceActivity.class);
                startActivity(intentForSettings);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}