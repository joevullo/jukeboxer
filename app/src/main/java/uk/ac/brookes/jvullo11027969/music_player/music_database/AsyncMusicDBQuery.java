package uk.ac.brookes.jvullo11027969.music_player.music_database;

/**
 * A class to help pass the selected query to {@link MusicDatabaseConnector()}
 * within the {@link AsyncMusicDBConnector} .
 *
 * Created by jvullo on 01/02/15.
 */
public class AsyncMusicDBQuery {

    // ints because I'm lazy and we can use them in a switch.
    public static final int GET_ALL_SONGS_MODE = 1;
    public static final int GET_ALL_ARTIST_MODE = 2;
    public static final int GET_ALL_ALBUM_MODE = 3;
    public static final int GET_ALL_PLAYLIST_MODE = 4;
    public static final int GET_ARTISTS_RELATED_ALBUMS = 5;
    public static final int GET_RECENT_ADDED_PLAYLIST = 7;
    public static final int SEARCH_FOR_ARTISTS = 9;
    public static final int SEARCH_FOR_ALBUMS = 11;
    public static final int SEARCH_FOR_ARTIST_ALBUMS = 12;
    public static final int SEARCH_FOR_SONG = 13;
    public static final int SEARCH_FOR_PLAYLIST = 14;
    public static final int GET_PLAYLIST_SONGS = 15;
    public static final int GET_ALBUM_OR_ARTIST_SONGS = 16;
    public static final int CREATE_NEW_PLAYLIST = 17;
    public static final int CREATE_NEW_PLAYLIST_WITH_IDS = 18;
    public static final int REMOVE_PLAYLIST_WITH_IDS = 19;

    private String searchQuery = "";
    private int commandType = 0;
    private long id = 0;

    private boolean searchQueryExists = false;
    private boolean idExists = false;

    private String playlistTitle = "";

    private long[] playlistAudioIDs;
    private long[] playlistIDs;

    public AsyncMusicDBQuery(String searchQuery, int commandType) {
        this.searchQuery = searchQuery;
        this.commandType = commandType;
        searchQueryExists = true;
    }

    public AsyncMusicDBQuery(long id, int commandType) {
        this.id = id;
        this.commandType = commandType;
        idExists = true;
    }

    public long[] getPlaylistIDs() {
        return playlistIDs;
    }

    public void setPlaylistIDs(long[] playlistIDs) {
        this.playlistIDs = playlistIDs;
    }

    public long[] getPlaylistAudioIDs() {
        return playlistAudioIDs;
    }

    public void setPlaylistAudioIDs(long[] playlistAudioIDs) {
        this.playlistAudioIDs = playlistAudioIDs;
    }

    public AsyncMusicDBQuery(int commandType) {
        this.commandType = commandType;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public int getCommandType() {
        return commandType;
    }

    public long getId() {
        return id;
    }

    public boolean isIdExists() {
        return idExists;
    }

    public boolean isSearchQueryExists() {
        return searchQueryExists;
    }

    public String getPlaylistTitle() {
        return playlistTitle;
    }

    public void setPlaylistTitle(String playlistTitle) {
        this.playlistTitle = playlistTitle;
    }
}
