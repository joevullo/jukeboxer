package uk.ac.brookes.jvullo11027969.music_player.music_database;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;

import java.io.FileDescriptor;
import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.music_player.music_objects.Album;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Artist;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Playlist;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Song;
import uk.ac.brookes.jvullo11027969.utils.Constants;
import uk.ac.brookes.jvullo11027969.utils.MyDebugger;

/**
 * This class contains methods to retrieve music.
 *
 * These methods will be used in the Fragments & SelectiveMusicList to
 * save on code dupe.
 *
 * Created by jvullo on 22/01/15.
 *
 *
 */
public class MusicDatabaseConnector {

    private ContentResolver musicResolver;

    public MusicDatabaseConnector(ContentResolver mContentResolver) {
        this.musicResolver = mContentResolver;
    }

    /**
     * Gets all songs on the device.
     * @return Returns an Arraylist <Songs> with these songs.
     */
    public ArrayList <Song> getAllSongs() {
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String orderBy = android.provider.MediaStore.Audio.Media.TITLE + " COLLATE NOCASE";
        return performSongResolverQuery(musicUri, null, null , null, orderBy);
    }

    /**
     * Gets all Albums as an ArrayList.
     * @return Returns an ArrayList.
    //     */
    public ArrayList <Album> getAllAlbums() {
        Uri musicUri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        String[] columns = {
                MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST,
                MediaStore.Audio.Albums.ALBUM_ART,
        };
        String orderBy = MediaStore.Audio.Albums.ALBUM + " COLLATE NOCASE";
        return performAlbumResolverQuery(musicUri, columns, null, null, orderBy);
    }

    /**
     * Gets all Artists as an ArrayList.
     * @return Returns an ArrayList.
     */
    public ArrayList <Artist> getAllArtists() {

        Uri musicUri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;// Media.EXTERNAL_CONTENT_URI;

        String[] columns = {
                MediaStore.Audio.Artists._ID,
                MediaStore.Audio.Artists.ARTIST,
                MediaStore.Audio.Artists.NUMBER_OF_ALBUMS,
        };

        String orderBy = MediaStore.Audio.Media.ARTIST + " COLLATE NOCASE";

        return performArtistResolverQuery(musicUri, columns, null, null, orderBy);
    }

    /**
     * Gets all playlists as an ArrayList.
     * @return Returns an ArrayList.
     */
    public ArrayList<Playlist>  getAllPlaylists() {

        ArrayList<Playlist> playlistsList = new ArrayList<Playlist>();
        playlistsList.add(new Playlist(Constants.RECENT_PLAYLIST_ID , "Recently Added"));

        // For the playlists fragment we are adding a recently added playlist

        Uri musicUri = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;

        final String idKey = MediaStore.Audio.Playlists._ID;
        final String nameKey = MediaStore.Audio.Playlists.NAME;
        final String[] columns = {nameKey, idKey};

        String  orderBy = MediaStore.Audio.Playlists.NAME + " COLLATE NOCASE";

        return performPlaylistResolverQuery(musicUri, columns, null, null, orderBy);
    }

    /**
     * Will return albums related to the ID of the artist passed in.
     *
     * @param artistId
     * @return
     */
    public ArrayList<Album> getArtistsRelatedAlbums(long artistId) {

        ArrayList<Album> albumList = new ArrayList<Album>();

        Uri musicUri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        String[] columns = {
                MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST,
                MediaStore.Audio.Albums.ALBUM_ART,
        };
        String orderBy = MediaStore.Audio.Albums.ALBUM + " COLLATE NOCASE";
        String where = MediaStore.Audio.Media.ARTIST_ID + "=?";
        String whereVal[] = {String.valueOf(artistId)};

        return performAlbumResolverQuery(musicUri, columns, where, whereVal, orderBy);
    }

    /**
     * Using the parameter which should be a artist's ID, this
     * method will get all the albums which are related to the
     * artists and return there ID's.
     *
     * @return Returns an ArrayList.
     */
    public long[] getArtistsAlbumIds(long artistId) {

        long[] artistAlbumIds;

        ArrayList<Album> albumList = getArtistsRelatedAlbums(artistId);

        artistAlbumIds = new long[albumList.size()];

        for (int i = 0; i < albumList.size(); i++) {
            artistAlbumIds[i] = albumList.get(i).getId();
        }

        return artistAlbumIds;
    }



    /**
     * A public getter that retrieves album artwork, but scales the image
     * by the height and width provided in the parameter.
     *
     * @param albumID - The Album ID to retrieve the artwork.
     * @param heightMod - The height to scale the artwork by.
     * @param widthMod - The width to scale the Artwork by.
     * @return Returns a Bitmap of the Artwork.
     */
    public Bitmap getScaledAlbumArtwork(long albumID, int heightMod, int widthMod) {
        Bitmap b = getAlbumArtwork(albumID);
        if (b != null)
            b = scaleBitmap(b, heightMod, widthMod);
        return b;
    }

    /**
     * Get the album artwork and return it as a bitmap.
     * This will retrieve a full sized version of the album artwork
     * with no scaling involved.
     *
     * @param albumId - The Album ID to retrieve the artwork.
     * @return
     */
    public Bitmap getAlbumArtwork(long albumId) {
        Bitmap bm = null;
        try
        {
            final Uri sArtworkUri = Uri
                    .parse("content://media/external/audio/albumart");

            Uri uri = ContentUris.withAppendedId(sArtworkUri, albumId);

            ParcelFileDescriptor pfd = musicResolver
                    .openFileDescriptor(uri, "r");

            if (pfd != null)
            {
                FileDescriptor fd = pfd.getFileDescriptor();
                bm = BitmapFactory.decodeFileDescriptor(fd);
            }
        } catch (Exception e) {
        }
        return bm;
    }

    /**
     * Resize a bitmap to a more efficient size to save data.
     */
    private Bitmap scaleBitmap(Bitmap image, int newHeight, int newWidth) {

        if (image == null) {
            MyDebugger.log(MyDebugger.TAG_MUSIC_DATABASE_CONNECTOR, "BitMap passed to scaleBitmap was null", null, "e");
            return null;
        }

        int width = image.getWidth();
        int height = image.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(image, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }

    /**
     * Generates a ArrayList of songs based on how recently they were
     * added to the device.
     *
     * @return
     */
    public ArrayList<Song> getRecentlyAddedPlaylistSongs() {
        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String orderBy = android.provider.MediaStore.Audio.Media.DATE_ADDED + " DESC Limit " + Constants.RECENTLY_ADDED_LIMIT;
        return performSongResolverQuery(musicUri, null, null , null, orderBy);

    }

    /**
     * A simple Playlist Resolver Query. This was implemented to save on code
     * duplication since many of the methods that returned Playlist used
     * almost the same code with minor differences to the query such
     * as where parameters.
     *
     * @param musicUri - Music URI.
     * @param columns - Columns,
     * @param where - Used in the query, where clause.
     * @param whereVal - Used in the query, vals for the where.
     * @param orderBy - Used in the query, the orderby.
     * @return
     */
    private ArrayList<Playlist> performPlaylistResolverQuery(Uri musicUri, String columns[], String where, String whereVal[] , String orderBy) {

        ArrayList<Playlist> playlistsList = new ArrayList<Playlist>();

        Cursor musicCursor = musicResolver.query(musicUri, columns, where, whereVal, orderBy);

        final String idKey = MediaStore.Audio.Playlists._ID;
        final String nameKey = MediaStore.Audio.Playlists.NAME;

        if (musicCursor != null && musicCursor.moveToFirst()) {
            do {
                long thisId = musicCursor.getLong(musicCursor.getColumnIndex(idKey));
                String thisTitle = musicCursor.getString(musicCursor.getColumnIndex(nameKey));
                playlistsList.add(new Playlist(thisId , thisTitle));
            }
            while (musicCursor.moveToNext());
        }

        musicCursor.close();
        return playlistsList;
    }


    /**
     * A simple Artist Resolver Query. This was implemented to save on code
     * duplication since many of the methods that returned Artist used
     * almost the same code with minor differences to the query such
     * as where parameters.
     *
     * @param musicUri - Music URI.
     * @param columns - Columns,
     * @param where - Used in the query, where clause.
     * @param whereVal - Used in the query, vals for the where.
     * @param orderBy - Used in the query, the orderby.
     * @return
     */
    private ArrayList<Artist> performArtistResolverQuery(Uri musicUri, String columns[], String where, String whereVal[] , String orderBy) {

        ArrayList<Artist> artistList = new ArrayList<Artist>();

        Cursor musicCursor = musicResolver.query(musicUri, columns, where, whereVal, orderBy);

        if (musicCursor != null && musicCursor.moveToFirst()) {
            int idColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Artists._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Artists.ARTIST);
            int numberOfAlbumsColumns = musicCursor.getColumnIndex
                    (MediaStore.Audio.Artists.NUMBER_OF_ALBUMS);

            do {
                long thisId = musicCursor.getLong(idColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                int numberOfAlbums = musicCursor.getInt(numberOfAlbumsColumns);
                artistList.add(new Artist(thisId, thisArtist, numberOfAlbums, getArtistsAlbumIds(thisId)));
            }
            while (musicCursor.moveToNext());
        }

        musicCursor.close();

        return artistList;
    }


    /**
     * A simple Album Resolver Query. This was implemented to save on code
     * duplication since many of the methods that returned Albums used
     * almost the same code with minor differences to the query such
     * as where parameters.
     *
     * @param musicUri - Music URI.
     * @param columns - Columns,
     * @param where - Used in the query, where clause.
     * @param whereVal - Used in the query, vals for the where.
     * @param orderBy - Used in the query, the orderby.
     * @return
     */
    private ArrayList<Album> performAlbumResolverQuery(Uri musicUri, String columns[], String where, String whereVal[] , String orderBy) {

        ArrayList<Album> albumList = new ArrayList<Album>();

        Cursor musicCursor = musicResolver.query(musicUri, columns, where, whereVal, orderBy);

        if (musicCursor != null && musicCursor.moveToFirst()) {
            int artistColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Albums.ARTIST);
            int albumColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Albums.ALBUM);
            int albumIdColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Albums._ID);

            do {
                String thisArtist = musicCursor.getString(artistColumn);
                String thisAlbum = musicCursor.getString(albumColumn);
                long thisAlbumId = musicCursor.getInt(albumIdColumn);

                albumList.add(new Album(thisAlbumId, thisAlbum, thisArtist));
            }
            while (musicCursor.moveToNext());
        }

        musicCursor.close();

        return albumList;
    }

        /**
         * A simple Song Resolver Query. This was implemented to save on code
         * duplication since many of the methods that returned songs used
         * almost the same code with minor differences to the query such
         * as where parameters.
         *
         * @param musicUri - Music URI.
         * @param columns - Columns,
         * @param where - Used in the query, where clause.
         * @param whereVal - Used in the query, vals for the where.
         * @param orderBy - Used in the query, the orderby.
         * @return
         */
    private ArrayList<Song> performSongResolverQuery(Uri musicUri, String columns[], String where, String whereVal[] , String orderBy) {

        ArrayList<Song> songList = new ArrayList<Song>();

        Cursor musicCursor = musicResolver.query(musicUri, columns, where, whereVal, orderBy);

        if (musicCursor != null && musicCursor.moveToFirst()) {
            int titleColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.ARTIST);
            int albumColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.ALBUM);
            int albumIdColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.ALBUM_ID);

            do {
                long thisId = musicCursor.getLong(idColumn);
                long thisAlbumId = musicCursor.getInt(albumIdColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                String thisAlbum = musicCursor.getString(albumColumn);

                Song s = new Song(thisId, thisTitle, thisArtist, thisAlbum, thisAlbumId);
                songList.add(s);

            }
            while (musicCursor.moveToNext());
        }

        musicCursor.close();
        return songList;
    }

    public ArrayList<Artist> searchForArtistsTitle(String artistTitle) {
        Uri musicUri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;// Media.EXTERNAL_CONTENT_URI;

        String[] columns = {
                MediaStore.Audio.Artists._ID,
                MediaStore.Audio.Artists.ARTIST,
                MediaStore.Audio.Artists.NUMBER_OF_ALBUMS,
        };

        String where = MediaStore.Audio.Artists.ARTIST +  " like ?" ;
        String[] whereVal = { "%" + artistTitle + "%"};

        String orderBy = MediaStore.Audio.Media.ARTIST + " COLLATE NOCASE";
        return performArtistResolverQuery(musicUri, columns, where, whereVal, orderBy);

    }

    public ArrayList<Album> searchForAlbumsTitle(String albumTitle) {
        Uri musicUri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        String[] columns = {
                MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST,
                MediaStore.Audio.Albums.ALBUM_ART,
        };

        String where = MediaStore.Audio.Albums.ALBUM +  " like ?" ;
        String[] whereVal = { "%" + albumTitle + "%"};

        String orderBy = MediaStore.Audio.Albums.ALBUM + " COLLATE NOCASE";

        return performAlbumResolverQuery(musicUri, columns, where, whereVal, orderBy);
    }

    public ArrayList<Artist> searchForArtistAlbums(String artistTitle) {
        Uri musicUri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;// Media.EXTERNAL_CONTENT_URI;

        String[] columns = {
                MediaStore.Audio.Artists._ID,
                MediaStore.Audio.Artists.ARTIST,
                MediaStore.Audio.Artists.NUMBER_OF_ALBUMS,
        };

        String where = MediaStore.Audio.Artists.ARTIST +  " like ?" ;
        String[] whereVal = { "%" + artistTitle + "%"};

        String orderBy = MediaStore.Audio.Media.ARTIST + " COLLATE NOCASE";
        return performArtistResolverQuery(musicUri, columns, where, whereVal, orderBy);
    }


    /**
     * This searches more song details than the norm, including the songs
     * artists and album details.
     *
     * @param songTitle
     * @return
     */
    public ArrayList<Song> detailedSearchForSong(String songTitle) {
        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String where = MediaStore.Audio.Media.ALBUM + " like ? OR "
                + MediaStore.Audio.Media.ARTIST + " like ? OR "
                + MediaStore.Audio.Media.TITLE + " like ? ";
        String[] whereVal = {"%" + songTitle + "%", "%" + songTitle + "%", "%" + songTitle + "%"};
        String orderBy = MediaStore.Audio.Media.ALBUM + " COLLATE NOCASE";

        return performSongResolverQuery(musicUri, null, where, whereVal, orderBy);
    }

    public ArrayList<Playlist> searchForPlaylistTitle(String playlistTitle) {

        ArrayList<Playlist> playlistsList = new ArrayList<Playlist>();
        playlistsList.add(new Playlist(Constants.RECENT_PLAYLIST_ID , "Recently Added"));

        // For the playlists fragment we are adding a recently added playlist

        Uri musicUri = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;

        final String idKey = MediaStore.Audio.Playlists._ID;
        final String nameKey = MediaStore.Audio.Playlists.NAME;
        final String[] columns = {nameKey, idKey};

        String  orderBy = MediaStore.Audio.Playlists.NAME + " COLLATE NOCASE";

        String where = MediaStore.Audio.Playlists.NAME + " like ?" ;
        String[] whereVal = {"%" + playlistTitle + "%"};

        return performPlaylistResolverQuery(musicUri, columns, where, whereVal, orderBy);
    }

    /**
     * Gets the songs on a playlist using the playlist ID which should
     * be based as a parameter.
     *
     * Precondition is that the ID passed matches a playlist ID.
     *
     * This doesn't use the song resolver method because songs do not store playlist ID's
     * in the same way they do album or artist ID's.
     *
     * @param playlistId - The ID of the playlist to return songs to.
     * @return Returns an arraylist of songs.
     */
    public ArrayList<Song> getPlaylistSongs(long playlistId) {

        ArrayList<Song> songList = new ArrayList<Song>();

        String[] columns = {
                MediaStore.Audio.Playlists.Members.ALBUM,
                MediaStore.Audio.Playlists.Members.ARTIST,
                MediaStore.Audio.Playlists.Members.TITLE,
                MediaStore.Audio.Playlists.Members.ALBUM_ID,
                MediaStore.Audio.Playlists.Members._ID,
                MediaStore.Audio.Playlists.Members.AUDIO_ID,
        };

        // Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        // Cursor musicCursor = musicResolver.query(musicUri, columns, where, whereVal, orderBy);
        Uri playlistUri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId);
        //Uri playlistUri = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
        Cursor cursor = musicResolver.query(playlistUri, columns, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int titleColumn = cursor.getColumnIndex
                    (MediaStore.Audio.Playlists.Members.TITLE);
            int playerIdColumn = cursor.getColumnIndex
                    (MediaStore.Audio.Playlists.Members._ID);
            int idColumn = cursor.getColumnIndex
                    (MediaStore.Audio.Playlists.Members.AUDIO_ID);
            int artistColumn = cursor.getColumnIndex
                    (MediaStore.Audio.Playlists.Members.ARTIST);
            int albumColumn = cursor.getColumnIndex
                    (MediaStore.Audio.Playlists.Members.ALBUM);
            int albumIdColumn = cursor.getColumnIndex
                    (MediaStore.Audio.Playlists.Members.ALBUM_ID);
            do {
                long thisId = cursor.getLong(idColumn);
                long thisAlbumId = cursor.getLong(albumIdColumn);
                String thisTitle = cursor.getString(titleColumn);
                String thisArtist = cursor.getString(artistColumn);
                String thisAlbum = cursor.getString(albumColumn);

                Song s = new Song(thisId, thisTitle, thisArtist, thisAlbum, thisAlbumId);

                // Don't store the album art as this would just waste memory, just make note that the Song has album art.
                // if (getAlbumArtwork(thisAlbumId) != null) {
                //    s.setHasAlbumArt(true);
                // }

                songList.add(s);

            }
            while (cursor.moveToNext());
        }

        return songList;
    }

    /**
     * TODO, could do with some refactoring.
     *
     * @param id
     * @param musicMode
     * @return
     */
    public ArrayList<Song> getAlbumOrArtistSongs(long id, String musicMode) {

        ArrayList<Song> songList = new ArrayList<Song>();

        String[] columns = {
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
        };

        String where = null;

        if (musicMode.equals(Constants.ALBUM_MODE)) {
            where = MediaStore.Audio.Media.ALBUM_ID + "=?";
        } else if (musicMode.equals(Constants.ARTIST_MODE)) {
            where = MediaStore.Audio.Media.ARTIST_ID + "=?";
        }

        String whereVal[] = {String.valueOf(id)};

        String orderBy = android.provider.MediaStore.Audio.Media.TITLE;

        return performSongResolverQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, columns, where, whereVal, orderBy);
    }

    public void removeListOfPlaylists(long[] playlistIDs) {
        for (int i = 0; i < playlistIDs.length; i++) {
            removePlaylist(playlistIDs[i]);
        }
    }

    public int removePlaylist(long playlistId) {
        int countDel = 0;

        try {
            Uri musicUri = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;

            String where = MediaStore.Audio.Playlists._ID + "=?" ; 
            String[] whereVal = { Long.toString(playlistId)};

            countDel = musicResolver.delete(musicUri, where, whereVal);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return countDel;
    }

    public void createPlaylistWithSongs(String name, long[] audioIDs) {

        if (name != null && name.length() > 0) {
            int id = idForplaylist(name);
            //int id = 1078;
            Uri uri;
            if (id >= 0) {
                uri = ContentUris.withAppendedId(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, id);
                //MusicUtils.clearPlaylist(CreatePlaylist.this, id);
            } else {

                // This creates the playlist.
                ContentValues values = new ContentValues(1);
                values.put(MediaStore.Audio.Playlists.NAME, name);
                uri = musicResolver.insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, values);

                long idLong = ContentUris.parseId(uri);
                // This should add the items to the new playlist.
                addListToPlaylist(audioIDs, idLong);
            }
        }
    }

    public void createPlaylistOnly(String name) {

        if (name != null && name.length() > 0) {
            int id = idForplaylist(name);
            Uri uri;
            if (id >= 0) {
                uri = ContentUris.withAppendedId(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, id);
                //MusicUtils.clearPlaylist(CreatePlaylist.this, id);
            } else {

                // This creates the playlist.
                ContentValues values = new ContentValues(1);
                values.put(MediaStore.Audio.Playlists.NAME, name);
                uri = musicResolver.insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, values);
            }
        }
    }

    private int idForplaylist(String name) {
        Cursor c = musicResolver.query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Audio.Playlists._ID },
                MediaStore.Audio.Playlists.NAME + "=?",
                new String[] { name },
                MediaStore.Audio.Playlists.NAME);

        int id = -1;
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                id = c.getInt(0);
            }
            c.close();
        }
        return id;
    }

    /**
     * Simply calls "add to playlist" in a for loop for the ints.
     * @param audioIDs
     * @param playlistID
     */
    public void addListToPlaylist(long[] audioIDs, long playlistID) {
        for (int i=0; i < audioIDs.length; i++) {
            addToPlaylist(audioIDs[i], playlistID);
        }
    }

    // TODO, not finished.
    public void addToPlaylist(long audioId, long playlistID) {
        String[] cols = new String[] { "count(*)" };
        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistID);
        Cursor cur = musicResolver.query(uri, cols, null, null, null);
        cur.moveToFirst();
        final int base = cur.getInt(0);
        cur.close();
        ContentValues values = new ContentValues();
        values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, Long.valueOf(base + audioId));
        values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
        musicResolver.insert(uri, values);
    }

    public void renamePlaylist(Context context, String newplaylist, long playlist_id) {
        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlist_id);
        ContentValues values = new ContentValues();
        String where = MediaStore.Audio.Playlists._ID + " =? ";
        String[] whereVal = { Long.toString(playlist_id) };
        values.put(MediaStore.Audio.Playlists.NAME, newplaylist);
        musicResolver.update(uri, values, where, whereVal);
    }

    private void deletePlaylist(int playlistID) {
        String where = MediaStore.Audio.Playlists._ID + "=?";
        String[] whereVal = {String.valueOf(playlistID)};
        musicResolver.delete(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, where, whereVal);
    }

    public int deletePlaylistTracks(Context context, long playlistId,
                                    long audioId) {
        ContentResolver resolver = context.getContentResolver();
        int countDel = 0;
        try {
            Uri uri = MediaStore.Audio.Playlists.Members.getContentUri(
                    "external", playlistId);
            String where = MediaStore.Audio.Playlists.Members._ID + "=?" ; 

            String audioId1 = Long.toString(audioId);
            String[] whereVal = { audioId1 };
            countDel = resolver.delete(uri, where,whereVal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countDel;

    }
}
