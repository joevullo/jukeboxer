package uk.ac.brookes.jvullo11027969.jukebox_api.api_objects_adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Song;
import uk.ac.brookes.jvullo11027969.utils.SpecialSelectableAdapter;

/**
 * Created by jvullo on 10/01/15.
 */
public class ApiSongAdapter extends SpecialSelectableAdapter {


    public ApiSongAdapter(Context c, ArrayList musicItems) {
        super(c , musicItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //map to song_item_view layout
        LinearLayout view = (LinearLayout) viewInflater.inflate
                (R.layout.song_item_view, parent, false);

        TextView songView = (TextView) view.findViewById(R.id.song_title);
        TextView artistView = (TextView) view.findViewById(R.id.song_artist);
        Song currSong = (Song) dataList.get(position);
        songView.setText(currSong.getTitle());
        artistView.setText(currSong.getArtist());

        //If the item is selected,
        setSelectedBackgroundView(view, position);

        //view.setTag(position);
        return view;
    }
}