package uk.ac.brookes.jvullo11027969.music_player.music_objects;

/**
 * Used to represent a song_item_view and create the view for it.
 *
 * Created by jvullo on 10/01/15.
 */
public class Artist extends AbstractMusic {

    private int noOfAlbum = 0;
    private long[] albumIds;

    public Artist(long id, String artist, int noOfAlbum, long albumIds[]) {

        this.id = id;
        this.artist = artist;
        this.noOfAlbum = noOfAlbum;
        this.albumIds = albumIds;
    }

    //Override the get title because the 'title' of the
    //is also its name in this case.
    @Override
    public String getTitle() {
        return artist;
    }

    public int getNoOfAlbum() {
        return noOfAlbum;
    }

    public long[] getAlbumIds() { return albumIds; }
}
