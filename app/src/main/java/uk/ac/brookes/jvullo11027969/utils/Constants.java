package uk.ac.brookes.jvullo11027969.utils;

/**
 * Created by jvullo on 30/11/14.
 */
public final class Constants {

    static final public int RECENTLY_ADDED_LIMIT = 50;

    // Keys for Items passed between activities.
    static final public String KEY_MUSIC_TYPE_MODE = "MUSIC_MODE"; // Identifies music type, used in SelectiveMusicList.
    static final public String KEY_MUSIC_ITEM_ID = "MUSIC_ITEM_ID";
    static final public String KEY_MUSIC_NAME = "MUSIC_NAME";

    // Modes for music
    static final public String SONG_MODE = "SONG";
    static final public String ARTIST_MODE = "ARTISTS";
    static final public String ALBUM_MODE = "ALBUMS";
    static final public String PLAYLIST_MODE = "PLAYLIST";
    static final public String UNKNOWN_MODE = "UNKNOWN";

    // This should match the string array in the strings.xml file.
    static final public String MUSIC_PLAYER_TAB = "Music Player";
    static final public String SHARE_TAB = "Share";
    static final public String JUKEBOX_TAB = "Jukebox";
    static final public String SOCIAL_TAB = "Socialise";

    // Could be tabs, could be items in a draw, used by either.
    static final public String[] MAIN_NAVIGATION_ITEMS = {MUSIC_PLAYER_TAB, SHARE_TAB, JUKEBOX_TAB, SOCIAL_TAB};

    // This should match the string array in the strings.xml file.
    static final public String PLAYLISTS_TAB_TAG = "PLAYLISTS";
    static final public String ALBUMS_TAB_TAG = "ALBUMS";
    static final public String ARTISTS_TAB_TAG = "ARTISTS";
    static final public String SONGS_TAB_TAG = "SONGS";

    // These need to be in the correct order.
    static final public String[] MUSIC_NAVIGATION_TABS = {SONGS_TAB_TAG , ARTISTS_TAB_TAG , ALBUMS_TAB_TAG , PLAYLISTS_TAB_TAG};

    static final public String FRIENDS_TAB_TAG = "FRIENDS";
    static final public String NEARBY_TAB_TAG = "NEARBY";

    static final public String[] SOCIAL_NAVIGATION_TABS = {FRIENDS_TAB_TAG , NEARBY_TAB_TAG };

    // ID to identify the
    static final public int RECENT_PLAYLIST_ID = 1;

}