package uk.ac.brookes.jvullo11027969.social;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import uk.ac.brookes.jvullo11027969.jukebox_api.reusable_api_fragments.FriendsFragmentBase;
import uk.ac.brookes.jvullo11027969.jukebox_api.reusable_api_fragments.NearbyUsersFragmentBase;
import uk.ac.brookes.jvullo11027969.main_activity.DefaultNavigationObjectFragment;

import static uk.ac.brookes.jvullo11027969.utils.Constants.SOCIAL_NAVIGATION_TABS;

/**
 * Created by jvullo on 02/12/14.
 */

public class SocialPagerAdapter extends FragmentPagerAdapter {

    public SocialPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public android.support.v4.app.Fragment getItem(int i) {

        switch (i) {
            case 0:
                return new FriendsFragmentBase();
            case 1:
                return new NearbyUsersFragmentBase();
            default:
                return new DefaultNavigationObjectFragment(); //Returns a default fragment in case the switch doesn't work out.
        }
    }

    @Override
    public int getCount() {
        return SOCIAL_NAVIGATION_TABS.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "" +  SOCIAL_NAVIGATION_TABS[position];
    }

}
