package uk.ac.brookes.jvullo11027969.music_player.music_interface;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.main_activity.AbstractMusicPlayerBase;
import uk.ac.brookes.jvullo11027969.main_activity.R;
import uk.ac.brookes.jvullo11027969.music_player.music_adapters.SongAdapter;
import uk.ac.brookes.jvullo11027969.music_player.music_database.MusicDatabaseConnector;
import uk.ac.brookes.jvullo11027969.utils.Constants;

/**
 * TODO This is currently exactly the same as SelectiveMusicList.
 */
public class SelectiveMusicListForPlaylists extends AbstractMusicPlayerBase implements AbsListView.OnItemClickListener {

    private long                        musicId;
    private String                      musicMode;
    private AbsListView                 mListView;
    private ArrayList                   musicList;
    protected BaseAdapter               musicAdapter;
    protected MusicDatabaseConnector mMDC;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selective_music_list);

        musicMode = getIntent().getExtras().getString(Constants.KEY_MUSIC_TYPE_MODE);
        musicId = getIntent().getExtras().getLong(Constants.KEY_MUSIC_ITEM_ID);
        String activityTitle = getIntent().getExtras().getString(Constants.KEY_MUSIC_NAME);

        setTitle(activityTitle); //Sets the actionbar title.
        TextView otherTitle = (TextView) findViewById(R.id.textView);
        otherTitle.setText(activityTitle);

        mListView = (AbsListView) findViewById(android.R.id.list);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);
        mListView.setEmptyView(findViewById(android.R.id.empty));

        mMDC = new MusicDatabaseConnector(getContentResolver());

        drawView();

        setMusicController();
    }

    public void drawView() {
        //songView = (ListView) rootView.findViewById(R.id.musicList);
        musicList = getMusicDataList(musicId);

        if (musicList.isEmpty()) {
            setEmptyText("Error: No music found.");
        } else {
            // The music adapter will be set by the subclasses.
            initialiseAdapter(musicList);
            // Set the adapter
            ((AdapterView<ListAdapter>) mListView).setAdapter(musicAdapter);
        }
    }

    public void initialiseAdapter(ArrayList musicList) {
        musicAdapter = new SongAdapter(this, musicList);
    }

    public ArrayList getMusicDataList(long id) {

        if (musicMode.equals(Constants.ALBUM_MODE) || musicMode.equals(Constants.ARTIST_MODE) ) {
            return mMDC.getAlbumOrArtistSongs(id,musicMode);
        } else if (musicMode.equals(Constants.PLAYLIST_MODE)) {
            if (id == Constants.RECENT_PLAYLIST_ID)
                return mMDC.getRecentlyAddedPlaylistSongs();
            else
                return mMDC.getPlaylistSongs(id);
        } else {
            return new ArrayList(); //Return an empty list.
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView)
            ((TextView) emptyView).setText(emptyText);
    }

    @Override
    public ViewGroup getAnchorPointView() {
        //return (ViewGroup) findViewById(android.R.id.list);
        return (ViewGroup) findViewById(R.id.content_frame);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_selective_music_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        JukeboxMainActivity a = (JukeboxMainActivity) getActivity();
//        a.setPlaylist(musicList);
//        a.playSong(1);

//        setPlaylist(musicList);

        startMusicPlayer(musicList, position);
    }

}