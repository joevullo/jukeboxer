package uk.ac.brookes.jvullo11027969.main_activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.util.ArrayList;

import uk.ac.brookes.jvullo11027969.main_activity.settings.UserPreferenceActivity;
import uk.ac.brookes.jvullo11027969.music_player.music_controls.MinimalMusicControllerView;
import uk.ac.brookes.jvullo11027969.music_player.music_objects.Song;

/**
 * A barebones class that just implements the connection between
 * a media controller and the music service.
 */
abstract public class AbstractMusicPlayerBase extends FragmentActivity implements MinimalMusicControllerView.MinMediaPlayerControl {

    private boolean                     musicBound = false;
    private boolean                     paused = false,
                                        playbackPaused = false;
    protected MusicService              mMusicService;
    private Intent                      playIntent;
    protected MinimalMusicControllerView  controller;

    private ServiceConnection musicConnection = new ServiceConnection(){

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.MusicBinder binder = (MusicService.MusicBinder)service;
            //get service
            mMusicService = binder.getService();
            //pass list
            //mMusicService.setList(songList);
            musicBound = true;

            showControllerIfPlaylist();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicBound = false;
        }
    };

    protected void showControllerIfPlaylist() {
        if (mMusicService.getCurrentSong() != null && controller != null) {
            controller.show(0);
            controller.updateControllerView();
        }
    }

    private void playNext(){
        mMusicService.playNextSong();
        if(playbackPaused) {
            setMusicController();
            playbackPaused = false;
        }
        controller.show(0);
        controller.updateControllerView();
    }

    private void playPrev(){
        mMusicService.playPreviousSong();
        if(playbackPaused) {
            setMusicController();
            playbackPaused = false;
        }
        controller.show(0);
        controller.updateControllerView();
    }

    public Song getCurrentSong() {
        if (mMusicService != null)
            return mMusicService.getCurrentSong();
        else
            return null;
    }

    public void startMusicPlayer(ArrayList<Song> newPlaylist, int startPosition) {
        mMusicService.startMusicPlayer(newPlaylist, startPosition);
        controller.show(0);
        controller.updateControllerView();
    }

    public void selectAndPlaySong(int newPosition) {
        mMusicService.selectAndPlaySong(newPosition);
        controller.show(0);
        controller.updateControllerView();
    }

    public void addToPlaylist(ArrayList<Song> playlist) {
        mMusicService.addToCurrentPlaylist(playlist);
    }

    // Methods to set and play music.
    public void setPlaylist(ArrayList<Song> playlist) {
        mMusicService.setPlaylist(playlist);
    }

    /**
     * Produce the anchor view for the music controller
     * to attach itself too.
     * @return
     */
    abstract public ViewGroup getAnchorPointView();

    //set the controller up
    protected void setMusicController(){
        controller = new MinimalMusicControllerView(this);
        //set and show
        controller.setMediaPlayer(this);
        if (getAnchorPointView() != null)
            controller.setAnchorView(getAnchorPointView());
        controller.setEnabled(true);
        //controller.setPadding(0, 0, 0, 20);
    }

    // Methods for the media controller

    @Override
    public boolean canPause() { return true; }

//    @Override
    public boolean canSeekBackward() { return true; }

//    @Override
    public boolean canSeekForward() { return true; }

//    @Override
//    public int getAudioSessionId() { return 0; }

//    @Override
    public int getBufferPercentage() { return 0; }

//    @Override
    public void seekTo(int pos) {
        mMusicService.seek(pos);
    }

    @Override
    public void start() {
        mMusicService.go();
    }

//    @Override
    public int getCurrentPosition() {
        if(mMusicService !=null && musicBound && mMusicService.isPng())
            return mMusicService.getPosn();
        else return 0;
    }

//    @Override
    public int getDuration() {
        if(mMusicService != null && musicBound && mMusicService.isPng())
            return mMusicService.getDur();
        else return 0;
    }

    public boolean isMusicBound() {
        return musicBound;
    }

    public MinimalMusicControllerView getMinMusicController() {
        return controller;
    }

    @Override
    public boolean isPlaying() {
        if(mMusicService != null && musicBound)
            return mMusicService.isPng();
        return false;
    }

    @Override
    public void pause() {
        playbackPaused = true;
        mMusicService.pausePlayer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_settings:
                Intent intentForSettings = new Intent(this, UserPreferenceActivity.class);
                startActivity(intentForSettings);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        paused = true;
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(paused) {
            setMusicController();
            paused = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(playIntent == null) {
            playIntent = new Intent(this, MusicService.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(musicConnection);
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        unbindService(musicConnection);
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        if(playIntent == null) {
//            playIntent = new Intent(this, MusicService.class);
//            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
//            startService(playIntent);
//        }
//    }

}
